import React, { Component } from 'react'
import Root from './src/Root'

// Redux config.
import { Provider } from 'react-redux'
import configureStore from './src/store'

const store = configureStore()

export default class App extends Component {
  render () {
    return (
      <Provider store={ store }>
        <Root />
      </Provider>
    )
  }
}
