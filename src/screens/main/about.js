import React, { Component } from 'react'
import { View, Text, Dimensions } from 'react-native'
import { Button, ThemeProvider } from 'react-native-elements'
import QRCode from 'react-native-qrcode'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import EStyleSheet from 'react-native-extended-stylesheet'
import CustomCard from '../../components/common/CustomCard'

// Global styles
import globalStyles from '../../utils/styles'
const theme = require('../../utils/theme.js')

const width = Dimensions.get('window').width

class About extends Component {
  constructor (props) { // eslint-disable-line no-useless-constructor
    super(props)
  }

  render () {
    const {
      container,
      titleContainer,
      infoContainer,
      infoSubContainer,
      qrContainer,
      title,
      textgh3,
      textg,
      did
    } = styles

    const {
      user,
      navigation
    } = this.props

    return (
      <ThemeProvider theme={theme}>
        <View >
          <View style={{ alignItems: 'center' }}>
            <CustomCard style={container}>
              <View style={titleContainer}>
                <Text style={title}>
                  User Information
                </Text>
              </View>

              <View style={infoContainer}>
                <View style={infoSubContainer}>
                  <Text style={textgh3}>Name</Text>
                  <Text style={textg}>
                    {user.name}
                    {user.last_name}
                  </Text>
                </View>

                <View style={infoSubContainer}>
                  <Text style={textgh3}>Handle</Text>
                  <Text style={textg}>{user.handler}</Text>
                </View>
              </View>

              <View style={qrContainer}>
                <View>
                  <QRCode
                    value={user.handler}
                    size={250}
                    bgColor='black'
                    fgColor='white'
                  />
                </View>
              </View>

              <View style={{ flex: 0.5, alignSelf: 'stretch', alignItems: 'center', justifyContent: 'center', margin: 15 }}>
                <Text style={did}>{user.did}</Text>
              </View>

              <View style={globalStyles.buttonContainerApp}>
                <Button
                  title='Log Out'
                  buttonStyle={globalStyles.buttonApp}
                  titleStyle={globalStyles.buttonTitleApp}
                  onPress={() => navigation.navigate('Home')}
                />
              </View>
            </CustomCard>
          </View>
        </View>
      </ThemeProvider>
    )
  }
}

const styles = EStyleSheet.create({
  container: {
    height: '90%',
    width: '90%',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '15%'
  },
  titleContainer: {
    flex: 1,
    alignSelf: 'stretch'
  },
  infoContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'stretch',
    marginLeft: 25,
    marginRight: 25
  },
  infoSubContainer: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'center',
    alignItems: 'center'
  },
  qrContainer: {
    flex: 4,
    margin: 10,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonStyle: {
    padding: 15,
    borderRadius: 10,
    backgroundColor: '#8E1D1A',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.25,
    shadowRadius: 6.84,
    elevation: 10
  },
  title: {
    fontFamily: 'Lato',
    color: 'gray',
    fontSize: '24rem',
    marginTop: '8%',
    textAlign: 'center'
  },
  textgh3: {
    marginTop: 15,
    fontFamily: 'Lato',
    color: '#333',
    fontSize: '20rem'
  },
  textg: {
    fontFamily: 'Lato',
    color: '#666',
    fontSize: '16rem'
  },
  did: {
    fontFamily: 'Lato',
    color: 'black',
    fontSize: '12rem'
  }
})

EStyleSheet.build({
  $rem: width / 370
})

About.propTypes = {
  user: PropTypes.object.isRequired
}

const mapStateToProps = state => ({ user: state.user })

export default connect(mapStateToProps, {})(About)
