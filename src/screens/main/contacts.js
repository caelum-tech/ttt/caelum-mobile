import React, { Component } from 'react'
import { View, Image, AsyncStorage, ScrollView } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'
import { withNavigationFocus } from 'react-navigation'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import ContactsService from '../../services/contacts'

import globalStyles from '../../utils/styles'

class ContactList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      contacts: []
    }
  }

  async UNSAFE_componentWillMount () { // eslint-disable-line camelcase
    this.loadContacts()
  }

  async componentDidUpdate (prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      this.loadContacts()
    }
  }

  async loadContacts () {
    await AsyncStorage.getItem('contacts').then(result => {
      this.setState({ contacts: JSON.parse(result) })
    })
  }

  onPress (contact) {
    this.props.navigation.navigate('MainTransactionsScreen', { handler: contact })
  }

  addNew (contact) {
    this.props.navigation.navigate('AddContactToList')
  }

  render () {
    const contacts = this.state.contacts
    return (
      <View style={globalStyles.baseContainer}>
        <View style={{ width: '100%', marginTop: 35, alignItems: 'center' }}>
          <Image style={{ alignContent: 'center', marginTop: 25 }} source={require('../../assets/contacts.png')} />
        </View>
        <ScrollView keyboardShouldPersistTaps="handled">
          <Divider />
          <ListItem
            leftAvatar={{ source: ContactsService.getAvatar('add') }}
            chevronColor="black"
            chevron
            title='Add new contact'
            subtitle='@new'
            containerStyle= {{ backgroundColor: '#FBFBFB' }}
            titleStyle = {globalStyles.textgb}
            subtitleStyle = {globalStyles.textg}
            onPress={() => this.addNew()}
          />
          {contacts.map((prop, key) => {
            return (
              <View key={key}>
                <Divider />
                <ListItem
                  leftAvatar={{ source: ContactsService.getAvatar(prop.handle) }}
                  chevronColor="black"
                  chevron
                  title={prop.name}
                  subtitle={'@' + prop.handle}
                  containerStyle= {{
                    backgroundColor: '#FBFBFB'
                  }}
                  titleStyle = {globalStyles.textgb}
                  subtitleStyle = {globalStyles.textg}
                  onPress={() => this.onPress(prop.handle)}
                />
              </View>
            )
          })}
        </ScrollView>
      </View>
    )
  }
}

ContactList.propTypes = {
  isFocused: PropTypes.bool.isRequired
}

const mapStateToProps = state => {
  const { contacts } = state.contacts
  return {
    contacts: contacts
  }
}

export default connect(mapStateToProps, {})(withNavigationFocus(ContactList))
