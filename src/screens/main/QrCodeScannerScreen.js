import React, { Component } from 'react'
import { CAELUM_WORKER_API_URL } from 'react-native-dotenv'

import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native'
import { BarCodeScanner } from 'expo-barcode-scanner'
import * as Permissions from 'expo-permissions'
import EStyleSheet from 'react-native-extended-stylesheet'
import { ethers } from 'ethers'

import UserService from '../../services/user'

import GS from '../../utils/styles'

// Touch this file to make sure the .env file is loaded for production compilation

class QrCodeScannerScreen extends Component {
  state = {
    loading: false,
    hasCameraPermission: null
  }

  async componentDidMount () {
    const { status } = await Permissions.askAsync(Permissions.CAMERA)
    this.setState({ hasCameraPermission: status === 'granted' })
  }

  render () {
    if (this.state.loading) {
      return (
        <View style={ GS.centeredContainer }>
          <ActivityIndicator />
        </View>
      )
    }

    const { hasCameraPermission } = this.state

    if (hasCameraPermission === null) {
      console.log('Requesting camera permission...')
      return <Text>Requesting camera permission</Text>
    }

    if (hasCameraPermission === false) {
      console.log('No access to camera')
      return <Text>No access to camera</Text>
    }

    console.log('Rendering BarCodeScanner...')

    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <View style={ styles.container }>
          <View style={ styles.qrContainer }>
            <BarCodeScanner
              onBarCodeScanned={this.handleBarCodeScanned}
              style={StyleSheet.absoluteFill}
            />
          </View>
          <View style={{ flex: 1, alignSelf: 'stretch', alignItems: 'center', justifyContent: 'center' }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Dapps')}>
              <Text style={{ color: '#000' }}>
                      Back
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  handleBarCodeScanned = async ({ type, data }) => {
    console.log('Scanned: ' + data)
    // Signing logic goes here.
    this.setState({ loading: true })
    // Load User
    const user = await UserService.loadUser()
    // Create Wallet object with privateKey & provider
    const signKey = new ethers.utils.SigningKey(user.privateKey)
    // Parse Socket ID before signing
    const messageBytes = ethers.utils.toUtf8Bytes(data)
    const messageDigest = ethers.utils.keccak256(messageBytes)
    // Sign a text message
    const signature = signKey.signDigest(messageDigest)

    const result = await fetch(CAELUM_WORKER_API_URL + '/sockets/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        handler: user.handler,
        messageDigest: messageDigest,
        signature: signature,
        socket: data,
        name: user.name
      })
    })

    console.log(`Socket login result: ${result.ok ? 'OK' : 'not OK'}`)

    this.setState({ loading: false })
    this.props.navigation.navigate('Dapps')
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.1)'
  },
  qrContainer: {
    flex: 1,
    marginTop: '60%',
    marginRight: '2%',
    marginLeft: '2%'
  }
})

EStyleSheet.build({
  // $rem: width / 370,
})

export default QrCodeScannerScreen
