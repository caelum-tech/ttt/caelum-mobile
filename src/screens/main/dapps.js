import React, { Component } from 'react'
import { View, Text, Image } from 'react-native'
import { ListItem, Divider } from 'react-native-elements'
import { withNavigationFocus } from 'react-navigation'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getPricesAndBalances } from '../../actions'
import TokenService from '../../services/token'
const globalStyles = require('../../utils/styles.js')

const styles = {
  container: {
    flex: 4,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  text: {
    fontSize: 20,
    fontFamily: 'Lato',
    color: 'grey'
  }
}

class Dapps extends Component {
  constructor (props) { // eslint-disable-line no-useless-constructor
    super(props)
  }

  componentDidMount () {
    this.props.getPricesAndBalances()
  }

  onPress (token, type) {
    this.props.navigation.navigate('MainTransactionsScreen',
      {
        token: token,
        type: type
      })
  }

  render () {
    const {
      ttcBalance,
      usdaBalance,
      tokens
    } = this.props
    return (
      <View style={{ flex: 1 }}>
        <View style={ styles.container }>
          <View style={{ width: '100%', marginTop: 35, marginBottom: 10, alignItems: 'center' }}>
            <Image style={{ alignContent: 'center', marginTop: 25 }} source={require('../../assets/ttc_token.png')} />
          </View>
          <Text style={globalStyles.textg}>Total Value of all Assets</Text>
          <Text style={globalStyles.textgh1}>$ { ((ttcBalance * 0.5) + usdaBalance).toFixed(2) }</Text>
          <Text style={globalStyles.textg}>TTC Price: $0.5</Text>
        </View>

        <View>
          {tokens.map((prop, key) => {
            return (
              <View key={key}>
                <Divider />
                <ListItem
                  leftAvatar={{ source: TokenService.getAvatar(prop.type, prop.token), style: { width: 60, height: 60 } }}

                  chevronColor="black"
                  chevron
                  title={prop.token.toUpperCase()}
                  subtitle={prop.balance.toString()}
                  containerStyle= {{
                    backgroundColor: '#FBFBFB'
                  }}
                  titleStyle = {globalStyles.textgb}
                  subtitleStyle = {globalStyles.textg}
                  onPress={() => this.onPress(prop.token, prop.type)}
                />
              </View>
            )
          })}
          <Divider style={{ marginBottom: 15 }}/>
        </View>
      </View>
    )
  }
}

Dapps.propTypes = {
  ttcBalance: PropTypes.number,
  usdaBalance: PropTypes.number,
  tokens: PropTypes.array.isRequired,
  contacts: PropTypes.array.isRequired,
  getPricesAndBalances: PropTypes.func
}

const mapStateToProps = state => {
  const { user } = state.user
  const { contacts } = state.contacts
  const { ttcBalance, usdaBalance, tokens } = state.pricesAndBalances
  return {
    user: user,
    contacts: contacts,
    ttcBalance: ttcBalance,
    usdaBalance: usdaBalance,
    tokens: tokens
  }
}

export default connect(mapStateToProps, { getPricesAndBalances })(withNavigationFocus(Dapps))
