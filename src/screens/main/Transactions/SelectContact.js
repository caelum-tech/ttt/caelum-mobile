import React, { Component } from 'react'
import { View, Image, ScrollView } from 'react-native'
import { ListItem } from 'react-native-elements'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import ContactService from '../../../services/contacts'

import {
  setAddress,
  setSelectedContact
} from '../../../actions'

class SelectContact extends Component {
  state = {
    contacts: []
  }

  UNSAFE_componentWillMount () { // eslint-disable-line camelcase
    ContactService.loadContacts()
      .then(c => this.setState({ contacts: c }))
  }

  setContact (index) {
    const contact = this.state.contacts[index]
    this.props.setSelectedContact(contact)
    this.props.navigation.navigate('MainTransactionsScreen')
  }

  render () {
    return (
      <View style={{ marginTop: 50, justifyContent: 'center', alignItems: 'center' }}>
        <View style={ styles.directoryContainer }>
          <View style={{ width: '100%', marginTop: 35, alignItems: 'center' }}>
            <Image style={{ alignContent: 'center', marginTop: 25 }} source={require('../../../assets/contacts.png')} />
          </View>
          <ScrollView keyboardShouldPersistTaps="handled">
            {
              this.state.contacts.map((l, i) => (
                <ListItem
                  key={i}
                  leftAvatar={{ source: ContactService.getAvatar(l.handle) }}
                  title={l.name}
                  subtitle={l.address}
                  titleStyle={{ fontWeight: 'bold' }}
                  onPress={() => this.setContact(i)}
                />
              ))
            }
          </ScrollView>
        </View>
      </View>
    )
  }
}

const styles = {
  directoryContainer: {
    height: 550,
    width: 350,
    justifyContent: 'center',
    margin: 25,
    marginLeft: 30
  },
  textStyle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#8E1D1A',
    marginLeft: 10,
    marginBottom: 15
  }
}

SelectContact.propTypes = {
  setSelectedContact: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  const { contacts, transactions } = state
  return {
    contacts: contacts.contacts,
    selectedContact: contacts.selectedContact,
    address: transactions.address
  }
}

export default connect(mapStateToProps, {
  setAddress,
  setSelectedContact
})(SelectContact)
