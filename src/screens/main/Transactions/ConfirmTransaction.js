import React, { Component } from 'react'
import { View, Text, Modal, StyleSheet, Image } from 'react-native'
import { Button } from 'react-native-elements'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import globalStyles from '../../../utils/styles'

class ConfirmTransaction extends Component {
  state = {
    contactName: null
  }

  // To improve: Connect to Transaction state so that
  // if txStatus === 'ERROR' => display error message.

  render () {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={true}
          onRequestClose={() => {} }
        >
          <View style={globalStyles.containerStyle}>

            <View style={{ width: '100%', marginTop: 35, alignItems: 'center' }}>
              <Image style={{ alignContent: 'center', marginTop: 25 }} source={require('../../../assets/success.png')} />
            </View>
            <Text style={globalStyles.textgh1}>Transfer done!</Text>
            <Button
              title='Send'
              onPress={() => this.sendTransaction()}
              buttonStyle={globalStyles.buttonApp}
              titleStyle={globalStyles.buttonTitleApp}
              disabled={this.state.notReady}
            />

            <View style={{ marginTop: 100, alignSelf: 'stretch' }}>
              {
                this.includesContact()
              }
            </View>
          </View>
        </Modal>
      </View>
    )
  }

  includesContact () {
    const contacts = this.props.contacts
    const handler = this.props.handler

    if (contacts.some(c => c.handler === handler)) {
      return (
        <View>
          <Button
            title='Great!'
            onPress={() => this.props.navigation.navigate('MainTransactionsScreen', { clean: true }) }
            buttonStyle={{ backgroundColor: '#8E1D1A', width: '95%', marginLeft: 10 }}
          />
        </View>
      )
    } else {
      return (
        <View>
          <Text style={[styles.textStyle, { color: 'gray' }]}> Do you want to save this contact to your contact list?</Text>
          <Button
            title='Save'
            onPress={() => this.props.navigation.navigate('AddContactToList')}
            buttonStyle={{ backgroundColor: '#8E1D1A', marginTop: 20, margin: 10 }}
          />
          <Button
            title='No thanks'
            onPress={() => this.props.navigation.navigate('MainTransactionsScreen') }
            buttonStyle={{ backgroundColor: 'transparent' }}
            titleStyle={{ color: '#8E1D1A' }}
          />
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    padding: 10
  },
  viewStyle: {
    flex: 1,
    height: 100,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 10
  },
  textStyle: {
    fontSize: 16,
    color: '#8E1D1A',
    marginLeft: 10,
    marginBottom: 15
  }
})

ConfirmTransaction.propTypes = {
  user: PropTypes.string,
  handler: PropTypes.string,
  contacts: PropTypes.array.isRequired
}

const mapStateToProps = state => {
  const { contacts } = state
  return {
    contacts: contacts.contacts,
    selectedContact: contacts.selectedContact
  }
}

export default connect(mapStateToProps, {})(ConfirmTransaction)
