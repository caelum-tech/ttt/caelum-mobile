import React, { Component } from 'react'
import {
  View,
  Text,
  Modal,
  ActivityIndicator,
  ScrollView
} from 'react-native'
import { ListItem } from 'react-native-elements'
import Icon from 'react-native-vector-icons/Feather'
import { withNavigationFocus } from 'react-navigation'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import {
  loadTxHistory
} from '../../../actions'

import HeaderWithBackButton from '../../../components/common/HeaderWithBackButton'

import styles from '../../../utils/styles'

class TxHistoryScreen extends Component {
  UNSAFE_componentWillMount () { // eslint-disable-line camelcase
    this.props.loadTxHistory()
  }

  componentDidUpdate (prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      this.props.loadTxHistory()
    }
  }

  render () {
    const navigation = this.props.navigation

    if (this.props.loading) {
      return (
        <View style={ styles.centeredContainer }>
          <ActivityIndicator />
        </View>
      )
    }

    return (
      <View style={ styles.simpleContainer }>
        <Modal animationType="slide" visible={true} onRequestClose={() => null}>

          <HeaderWithBackButton
            onBackButtonPress={ () => navigation.navigate('MainTransactionsScreen') }>
                Transaction List
          </HeaderWithBackButton>

          <View style={{ flex: 8 }}>
            <ScrollView keyboardShouldPersistTaps="handled">
              {
                this.props.txHistory.reverse().map((tx, i) => (
                  <ListItem
                    key={i}
                    leftIcon={
                      tx.type === 'receive'
                        ? <Icon name="log-in" size={30} color={'#004225'}/>
                        : <Icon name="log-out" size={30} color={'#8E1D1A'}/>}
                    title={ '@' + tx.handler2 }
                    subtitle={
                      tx.read ? 'Confirmed'
                        : <Text style={ styles.textBoldRed14 }>new!</Text> }
                    rightTitle={ tx.value }
                    rightSubtitle={ tx.token }
                    rightTitleStyle={{ fontWeight: 'bold', color: '#8E1D1A' }}
                    titleStyle={{ fontWeight: 'bold' }}
                    containerStyle={ styles.listItem }
                    onPress={() => navigation.navigate('txDetail', { tx: tx })}
                  />
                ))
              }
            </ScrollView>
          </View>
        </Modal>
      </View>
    )
  }
}

TxHistoryScreen.propTypes = {
  user: PropTypes.object,
  isFocused: PropTypes.bool,
  loading: PropTypes.bool.isRequired,
  txHistory: PropTypes.node.isRequired,
  loadTxHistory: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  const { user } = state.user
  const { transactionList, loading } = state.txHistory
  return {
    user: user,
    txHistory: transactionList,
    loading: loading
  }
}

export default connect(mapStateToProps, {
  loadTxHistory
})(withNavigationFocus(TxHistoryScreen))
