import React, { Component } from 'react'
import { View, ScrollView, Image, Modal, Text } from 'react-native'
import { Button, Input, ListItem, Card, ThemeProvider } from 'react-native-elements'
import { withNavigationFocus } from 'react-navigation'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import TokenService from '../../../services/token'
import ContactsService from '../../../services/contacts'

// Actions
import {
  cleanReduxState,
  setHandler,
  setToken,
  setAmount,
  setSelectedContact,
  initiateTransaction
} from '../../../actions'

// Global styles
import globalStyles from '../../../utils/styles'
const theme = require('../../../utils/theme.js')

class MainTransactionsScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      showModal: false,
      token: false,
      handler: false,
      amount: 0,
      notReady: false
    }
  }

  isReady () {
    if (this.state.handler !== false &&
      this.state.token !== false &&
      this.state.amount !== 0) {
      this.setState({ notReady: false })
      return true
    } else {
      this.setState({ notReady: true })
      return false
    }
  }

  async UNSAFE_componentWillMount () { // eslint-disable-line camelcase
    if (this.props.selectedContact) {
      this.props.setHandler(this.props.selectedContact.handle)
    }

    if (this.props.navigation.getParam('clean')) {
      this.resetState()
    }
  }

  UNSAFE_componentWillReceiveProps (nextProps) { // eslint-disable-line camelcase
    if (this.props.navigation.getParam('handler')) {
      this.setState({ handler: this.props.navigation.getParam('handler') })
    }
    if (this.props.navigation.getParam('token')) {
      this.setState({ token: this.props.navigation.getParam('token') })
      this.setState({ type: this.props.navigation.getParam('type') })
    }
    if (nextProps.txStatus === 'CONFIRMED') {
      this.props.navigation.navigate('ConfirmTransaction')
    }
  }

  resetState () {
    // Clean contacts state.
    this.props.setSelectedContact(false)
    // Clean transactions state.
    this.props.cleanReduxState()
    // Reset showModal to false.
    this.setState({ showModal: false })
  }

  valueToSend (value) {
    this.setState({ amount: value })
  }

  async sendTransaction () {
    if (!this.isReady()) {
      return
    }
    this.props.setToken(this.state.token)
    this.props.setHandler(this.state.handler)
    this.props.setAmount(Number(this.state.amount))
    // Show loading modal.
    this.setState({ showModal: true })
    // Initiate tx Saga.
    this.props.initiateTransaction()
  }

  render () {
    return (
      <ThemeProvider theme={theme}>
        <ScrollView keyboardShouldPersistTaps="handled" style={globalStyles.containerStyle}>
          <View style={{ width: '100%', alignItems: 'center' }}>
            <Image style={{ alignContent: 'center' }} source={require('../../../assets/transfer.png')} />
          </View>
          <Card title="Select Token">
            { this.state.token
              ? <ListItem
                leftAvatar={{ source: TokenService.getAvatar(this.state.type, this.state.token) }}
                chevronColor="black"
                chevron
                title='Token'
                subtitle={'@' + this.state.token}
                containerStyle= {{ backgroundColor: '#FBFBFB' }}
                titleStyle = {globalStyles.textgb}
                subtitleStyle = {globalStyles.textg}
                onPress={() => this.props.navigation.navigate('Dapps')}
              />
              : <ListItem
                leftAvatar={{ source: TokenService.getAvatar('add') }}
                chevronColor="black"
                chevron
                title='Select Token'
                subtitle='@new'
                containerStyle= {{ backgroundColor: '#FBFBFB' }}
                titleStyle = {globalStyles.textgb}
                subtitleStyle = {globalStyles.textg}
                onPress={() => this.props.navigation.navigate('Dapps')}
              />
            }
          </Card>
          <Card title="Select Contact (To)">
            { this.state.handler
              ? <ListItem
                leftAvatar={{ source: ContactsService.getAvatar(this.state.handler) }}
                chevronColor="black"
                chevron
                title='Send to User'
                subtitle={'@' + this.state.handler}
                containerStyle= {{ backgroundColor: '#FBFBFB' }}
                titleStyle = {globalStyles.textgb}
                subtitleStyle = {globalStyles.textg}
                onPress={() => this.props.navigation.navigate('Contacts')}
              />
              : <ListItem
                leftAvatar={{ source: ContactsService.getAvatar('add') }}
                chevronColor="black"
                chevron
                title='Select Contact'
                subtitle='@new'
                containerStyle= {{ backgroundColor: '#FBFBFB' }}
                titleStyle = {globalStyles.textgb}
                subtitleStyle = {globalStyles.textg}
                onPress={() => this.props.navigation.navigate('Contacts')}
              />
            }
          </Card>

          <Card title="Amount to Send">
            <View style={globalStyles.inputContainerApp}>
              <Input
                label=""
                value={ `${Number(this.state.amount)}` }
                placeholder='0'
                keyboardType="numeric"
                inputStyle={globalStyles.inputApp}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                selectionColor='#FFFFFF'
                underlineColorAndroid='transparent'
                onChangeText={ value => this.valueToSend(value) }
              />
            </View>
          </Card>

          <View style={[globalStyles.buttonContainerApp, { marginBottom: 70 }]}>
            {this.state.notReady &&
            <View style={{ marginTop: 10, alignItems: 'center' }}>
              <Text style={{ color: 'red' }}>All entries are required.</Text>
            </View>
            }
            <Button title='Send'
              onPress={() => this.sendTransaction()}
              buttonStyle={globalStyles.buttonApp}
              titleStyle={globalStyles.buttonTitleApp}
            />
          </View>

          <Modal animationType="slide" visible={this.state.showModal} onRequestClose={() => null}>
            <View style={[globalStyles.containerStyle, { justifyContent: 'center', alignItems: 'center', width: '100%', height: 300 }]}>
              <Image style={{ width: 200, height: 200 }} source={require('../../../assets/loading.gif')} />
            </View>
          </Modal>
        </ScrollView>
      </ThemeProvider>
    )
  }
}

MainTransactionsScreen.propTypes = {
  user: PropTypes.object.isRequired,
  handler: PropTypes.string,
  token: PropTypes.string,
  amount: PropTypes.number,
  txStatus: PropTypes.node,
  selectedContact: PropTypes.any,
  cleanReduxState: PropTypes.func,
  setHandler: PropTypes.func,
  setToken: PropTypes.func,
  initiateTransaction: PropTypes.func,
  setAmount: PropTypes.func,
  setSelectedContact: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  const { contacts, user } = state
  const { handler, token, amount, txStatus } = state.transactions
  return {
    user,
    handler,
    token,
    amount,
    txStatus,
    // TODO: remove unnecessary wrapper
    contacts: contacts.contacts,
    selectedContact: contacts.selectedContact
  }
}

export default connect(mapStateToProps, {
  cleanReduxState,
  setHandler,
  setToken,
  setAmount,
  setSelectedContact,
  initiateTransaction
})(withNavigationFocus(MainTransactionsScreen))
