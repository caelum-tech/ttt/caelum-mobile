import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions
} from 'react-native'
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/Feather'
import { withNavigationFocus } from 'react-navigation'
import { connect } from 'react-redux'
import moment from 'moment'
import EStyleSheet from 'react-native-extended-stylesheet'
import PropTypes from 'prop-types'

// Components
import PopUp from '../../../components/common/PopUp'
import CustomCard from '../../../components/common/CustomCard'

// Utils
import styles from '../../../utils/styles'

const width = Dimensions.get('window').width
const iconSize = width / 12

class txDetail extends Component {
  state = {
    tx: null
  }

  UNSAFE_componentWillMount () { // eslint-disable-line camelcase
    if (this.props.navigation.getParam('tx')) {
      const tx = this.props.navigation.getParam('tx')
      this.setState({ tx: tx })
    }
  }

  checkIfContactExists (tx) {
    const { contacts } = this.props
    if (!contacts.some(c => c.handle === tx.handler2)) {
      return (
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', marginLeft: '7%' }}>
          <CustomCard elevated={true} style={ compStyles.addContactButton }>
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('AddContactToList', { handler: tx.handler2 }) }}>
              <Icon name="plus" size={iconSize} color={'#8E1D1A'} />
            </TouchableOpacity>
          </CustomCard>
          <View style={{ alignItems: 'center', marginLeft: '5%' }}>
            <Text style={[styles.titleStyleOne, compStyles.textBody, { marginBottom: '3%', fontWeight: 'bold', color: '#000' }]}>Add
              <Text style={{ color: '#8E1D1A' }}>{' @' + tx.handler2.charAt(0).toUpperCase() + tx.handler2.slice(1) + '    '}
              </Text></Text>
          </View>
        </View>
      )
    }
  }

  render () {
    return (
      <View>
        <PopUp iconName={'bookmark'} title={'  Transaction Details'}>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 1, justifyContent: 'center', marginLeft: '7%' }}>
                <Text style={[styles.titleStyleOne, compStyles.textBody]}>From:</Text>
              </View>
              <View style={{ flex: 2, justifyContent: 'center' }}>
                <Text style={[styles.titleStyleOne, compStyles.textBody, compStyles.handles]}>
                @{
                    this.state.tx.type === 'receive'
                      ? this.state.tx.handler2
                      : this.state.tx.handler
                  }
                </Text>
              </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 1, justifyContent: 'center', marginLeft: '7%' }}>
                <Text style={[styles.titleStyleOne, compStyles.textBody]}>To:</Text>
              </View>
              <View style={{ flex: 2, justifyContent: 'center' }}>
                <Text style={[styles.titleStyleOne, compStyles.textBody, compStyles.handles]}>
                @{
                    this.state.tx.type === 'receive'
                      ? this.state.tx.handler
                      : this.state.tx.handler2
                  }
                </Text>
              </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 1, justifyContent: 'center', marginLeft: '7%' }}>
                <Text style={[styles.titleStyleOne, compStyles.textBody]}>Amount:</Text>
              </View>
              <View style={{ flex: 2, justifyContent: 'center' }}>
                <Text style={[styles.titleStyleOne, compStyles.amount]}>{ this.state.tx.value + ' '}
                  <Text style={ compStyles.token }>{this.state.tx.token}</Text></Text>
              </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 1, justifyContent: 'center', marginLeft: '7%' }}>
                <Text style={[styles.titleStyleOne, compStyles.textBody]}>Date:</Text>
              </View>
              <View style={{ flex: 3, justifyContent: 'center' }}>
                <Text style={ compStyles.dateStyle}>{moment(this.state.tx.createdAt).format('L LTS')}</Text>
              </View>
            </View>

            <View style={{ flex: 1.5, flexDirection: 'row' }}>
              {
                this.checkIfContactExists(this.state.tx)
              }
            </View>

            <View style={{ flex: 1.5, paddingLeft: 10, paddingRight: 10, paddingBottom: 10, justifyContent: 'center' }}>
              <Button
                title='OK'
                onPress={() => this.props.navigation.navigate('TxHistoryScreen')}
                buttonStyle={ styles.buttonStyle }
              />
            </View>
          </View>
        </PopUp>
      </View>
    )
  }
}

const compStyles = EStyleSheet.create({
  textStyle: {
    fontSize: '1rem',
    fontWeight: 'bold',
    color: 'black'
  },
  addContactButton: {
    height: iconSize + 10,
    width: iconSize + 10,
    borderRadius: (iconSize + 10 / 2),
    alignItems: 'center',
    justifyContent: 'center'
  },
  textBody: {
    fontSize: '1.1rem',
    alignSelf: 'flex-start'
  },
  dateStyle: {
    color: 'gray',
    alignSelf: 'flex-start',
    fontSize: '1rem'
  },
  handles: {
    color: 'gray',
    alignSelf: 'flex-end',
    fontSize: '1.2rem',
    marginRight: '15%'
  },
  amount: {
    fontSize: '1.2rem',
    fontWeight: 'bold',
    marginRight: '15%',
    color: '#8E1D1A',
    alignSelf: 'flex-end'
  },
  token: {
    color: 'gray',
    fontSize: '1rem'
  }
})

EStyleSheet.build({
  $rem: width / 370
})

txDetail.propTypes = {
  contacts: PropTypes.array
}

const mapStateToProps = state => {
  const { user } = state.user
  const { contacts } = state.contacts
  return {
    user: user,
    contacts: contacts
  }
}

export default connect(mapStateToProps, {})(withNavigationFocus(txDetail))
