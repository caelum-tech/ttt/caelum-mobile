import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import { BarCodeScanner } from 'expo-barcode-scanner'
import * as Permissions from 'expo-permissions'

class QrCodeScanner extends Component {
  state = {
    hasCameraPermission: null
  }

  async componentDidMount () {
    const { status } = await Permissions.askAsync(Permissions.CAMERA)
    this.setState({ hasCameraPermission: status === 'granted' })
  }

  render () {
    const { hasCameraPermission } = this.state

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>
    }

    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>
    }

    return (
      <View style={ styles.container }>
        <View style={ styles.qrContainer }>
          <BarCodeScanner
            onBarCodeScanned={this.handleBarCodeScanned}
            style={StyleSheet.absoluteFill}
          />
        </View>
        <View style={{ flex: 1, alignSelf: 'stretch', alignItems: 'center', justifyContent: 'center' }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('MainTransactionsScreen')}>
            <Text style={{ color: '#fff' }}>
                    Back
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.props.navigation.navigate('MainTransactionsScreen', { handler: data })
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#000'
  },
  qrContainer: {
    flex: 1,
    marginLeft: 25,
    marginRight: 25,
    marginTop: 250,
    marginBottom: 250
  }
}

export default QrCodeScanner
