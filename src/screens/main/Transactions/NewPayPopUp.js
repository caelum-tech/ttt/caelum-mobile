import React, { Component } from 'react'
import { withNavigationFocus } from 'react-navigation'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

import EthService from '../../../services/eth'
import UserService from '../../../services/user'

// Actions
import {
  setPayment,
  initiatePayment
} from '../../../actions'

// Components
import NewPaymentPopUp from '../../../components/NewPaymentPopUp'

class NewPayPopUp extends Component {
  state = {
    transferring: false
  };

  transfer = async function () {
    console.log('NewPayPopUp.transfer state:')
    console.log(this.state)
    if (this.state.transferring) {
      console.log('NewPayPopUp.transfer: already transferring, ignored')
      return
    } else {
      console.log('NewPayPopUp.transfer: now transferring')
      this.setState({ transferring: true })
    }
    try {
      console.log('================================================')
      console.log(this.props.newPay)
      const tokenAddress = await UserService.loadTokens(this.props.newPay.payToken)
      console.log('TOKEN ttc')
      console.log(tokenAddress)

      // Get Receiver DID.
      const toDid = await UserService.resolveUserDid(this.props.newPay.seller)
      console.log(this.props.newPay.seller)
      console.log('Root DID')
      console.log(toDid)

      // Build and sign transaction.
      const txData = await EthService.getTransfer(tokenAddress, toDid, this.props.newPay.payAmount)
      console.log('txData')
      console.log(txData)

      const txStatus = await EthService.sendPayment(txData, this.props.newPay)
      console.log('send TX')
      console.log(txStatus)

      if (txStatus.success) {
        this.props.navigation.navigate('Dapps')
      } // else TODO popup error
    } finally {
      // can't do this: setting state after component already gone.
      // Unnecessary in any case.
      // this.setState({ transferring: false })
      console.log('NewPayPopUp.transfer: transferring completed')
    }
  }

  render () {
    const pay = this.props.newPay
    console.log('NewPayPopUp.render state:')
    console.log(this.state)
    // Deconstruct contacts
    return (
      <NewPaymentPopUp
        pay={ pay }
        firstButtonFunction={() => this.props.navigation.navigate('Dapps')}
        secondButtonFunction={() => this.transfer()}
      />
    )
  }
}

NewPayPopUp.propTypes = {
  newPay: PropTypes.object.isRequired,
  txStatus: PropTypes.node,
  transferring: PropTypes.bool
}

const mapStateToProps = state => {
  const { contacts } = state.contacts
  const { newPay } = state.newPay

  return {
    //    txStatus,
    contacts: contacts,
    newPay: newPay
  }
}

export default connect(mapStateToProps, {
  // cleanReduxState,
  setPayment,
  // setAmount,
  // setSelectedContact,
  initiatePayment
})(withNavigationFocus(NewPayPopUp))
