import React, { Component } from 'react'
import { View, Text, AsyncStorage } from 'react-native'
import { Input, Button, Divider } from 'react-native-elements'
import { connect } from 'react-redux'
import { withNavigation } from 'react-navigation'
import globalStyles from '../../../utils/styles'
import ContactService from '../../../services/contacts'
import PropTypes from 'prop-types'

class AddContactToList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      name: '',
      handler: props.handler,
      nameReady: false,
      handlerReady: false,
      notReady: true
    }
  }

  setName (name) {
    this.setState({ nameReady: false })
    if (name.trim().length > 0) {
      this.setState({ nameReady: true })
    }
    this.setState({ name: name.trim() })
  }

  setHandler (handler) {
    this.setState({ handlerReady: false })
    if (handler.trim().length > 0) {
      ContactService.getContact(handler)
        .then(async (contact) => {
          console.log(contact)
          if (contact.success) {
            this.setState({ handlerReady: true })
          }
        })
    }
    this.setState({ handler: handler.trim() })
  }

  async saveContact () {
    ContactService.loadContacts()
      .then(async (contacts) => {
        const contactsArray = contacts
        contactsArray.push({
          name: this.state.name.trim(),
          handle: this.state.handler.trim(),
          icon: ''
        })
        await AsyncStorage.setItem('contacts', JSON.stringify(contactsArray))
        this.props.navigation.navigate('Dapps')
      })
  }

  async cancel () {
    this.props.navigation.navigate('Dapps')
  }

  render () {
    return (
      <View style={globalStyles.containerStyle}>
        <Text style={globalStyles.textgh1}>Add a new Contact</Text>
        <View style={ globalStyles.formContainer }>
          <Divider />
          <Input
            label="Contact Name"
            placeholder="Name..."
            textContentType="none"
            keyboardType="default"
            value={ this.state.name }
            onChangeText={ value => this.setName(value) }
            inputStyle={globalStyles.TextInputStyleClass}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            containerStyle={{ justifyContent: 'center', width: '85%' }}
            selectionColor='#FFFFFF'
            underlineColorAndroid='transparent'
          />
          <Divider />
          <Input
            label="Handler"
            placeholder="Handler..."
            value={ this.state.handler }
            textContentType="none"
            keyboardType="default"
            onChangeText={ value => this.setHandler(value) }
            inputStyle={globalStyles.TextInputStyleClass}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            containerStyle={{ justifyContent: 'center', width: '85%', marginTop: 25 }}
            selectionColor='#FFFFFF'
            underlineColorAndroid='transparent'
          />
          <Divider />
          <Button
            title='Save'
            onPress={() => this.saveContact()}
            buttonStyle={[globalStyles.buttonApp, { marginTop: 20, width: 150 }]}
            titleStyle={globalStyles.buttonTitleApp}
            disabled={!this.state.nameReady || !this.state.handlerReady}
          />
          <Button
            title='Cancel'
            onPress={() => this.cancel()}
            buttonStyle={[globalStyles.buttonApp, { marginTop: 20, width: 150 }]}
            titleStyle={globalStyles.buttonTitleApp}
          />
        </View>
      </View>
    )
  }
}

AddContactToList.propTypes = {
  handler: PropTypes.string
}

const mapStateToProps = state => {
  // there are some issues with the scope on newTx
  // the first case is when there's an existing newTx, the second is when there isn't.
  const { name2, handler2 } = state.newTx.newTx ? state.newTx.newTx : state.newTx
  return {
    name: name2,
    handler: handler2
  }
}
export default connect(mapStateToProps, {})(withNavigation(AddContactToList))
