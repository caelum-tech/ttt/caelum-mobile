import React, { Component } from 'react'
import { withNavigationFocus } from 'react-navigation'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

// Components
import NewTransactionPopUp from '../../../components/NewTransactionPopUp'
import NewTransactionUnknownUser from '../../../components/NewTransactionUnknownUser'

class NewTxPopUp extends Component {
  render () {
    const tx = this.props.newTx
    // Deconstruct contacts
    const contacts = this.props.contacts
    // If sender is not in contact list,
    if (!contacts.some(c => c.handle === tx.handler2)) {
      // return multi-option Pop Up
      return (
        <NewTransactionUnknownUser
          tx={ tx }
          firstButtonFunction={
            () => this.props.navigation.navigate('AddContactToList', { handler: tx.handler2 })}
          secondButtonFunction={
            () => this.props.navigation.navigate('txDetail', { tx: tx })}
          thirdButtonFunction={
            () => this.props.navigation.navigate('Dapps')}
        />
      )
    } else {
      // If sender already exists in contact list, render normal Pop Up.
      return (
        <NewTransactionPopUp
          tx={ tx }
          firstButtonFunction={
            () => this.props.navigation.navigate('Dapps')}
          secondButtonFunction={
            () => this.props.navigation.navigate('txDetail', { tx: tx })}
        />
      )
    }
  }
}

NewTxPopUp.propTypes = {
  newTx: PropTypes.object.isRequired,
  contacts: PropTypes.array.isRequired
}

const mapStateToProps = state => {
  const { contacts } = state.contacts
  const { newTx } = state.newTx
  return {
    contacts: contacts,
    newTx: newTx
  }
}

export default connect(mapStateToProps, {})(withNavigationFocus(NewTxPopUp))
