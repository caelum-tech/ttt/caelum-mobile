import React from 'react'
import { AppLoading, Notifications } from 'expo'
import { Asset } from 'expo-asset'
import * as Font from 'expo-font'
import { AsyncStorage, View, Text, Image } from 'react-native'
import { withNavigationFocus } from 'react-navigation'
import { Button, ThemeProvider } from 'react-native-elements'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

// Services
import UserService from '../services/user.js'
import ContactsService from '../services/contacts.js'

// Actions
import {
  setUser,
  setContacts,
  setnewTx,
  setnewPay,
  newTransfer,
  getPricesAndBalances,
  loadAppAndBackendVersions
} from '../actions'

import Directory from '../assets/contacts.json'

const styles = require('../utils/styles.js')
const theme = require('../utils/theme.js')

class Home extends React.Component {
  static navigationOptions = { header: null };

  constructor (props) {
    super(props)
    this.state = {
      isLoadingComplete: false,
      user: false
    }
  }

  async componentDidMount () {
    // Fire Saga to check if backend and app versions math.
    this.props.loadAppAndBackendVersions()

    // Load User & Contacts.
    const user = await UserService.loadUser()
    const contacts = await ContactsService.loadContacts()

    // If a user already exists,
    if (user) {
      // set user in redux state,
      this.props.setUser(user)
      // set its contacts,
      this.props.setContacts(contacts)
    } else {
      // If not, create new dummmy conctacts,
      await AsyncStorage.setItem('contacts', JSON.stringify(Directory))
    }

    // TODO: Set user in Redux state, not component state.
    this.setState({ user })
    this.setState({ mkey: (user !== false) ? 'USER' : 'Not found' })

    this._notificationSubscription = Notifications.addListener(this._handleNotification)
  }

  componentDidUpdate (prevProps) {
    if (prevProps.isFocused !== this.props.isFocused) {
      this.loadUser()
    }
  }

  loadUser () {
    // Not in use anymore
    UserService.loadUser().then((value) => {
      this.setState({ user: value })
      this.setState({ mkey: (value !== false) ? 'USER' : 'Not found' })
    })
  }

  async reset () {
    console.log('RESET')
    this.setState({ user: false })
    await AsyncStorage.setItem('contacts', JSON.stringify(Directory))
    this.props.navigation.navigate('Register')
  }

  render () {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      )
    } else {
      return (
        <ThemeProvider theme={theme}>
          <View style={{ flex: 1 }}>
            <Image source={require('../assets/background.png')} style={styles.backgroundContainer} />
            <View style={styles.container}>
              <View style={{ flex: 4 }} />
              <View style={styles.home}>
                <View style={styles.bottom}>
                  <View style={{ alignItems: 'center' }}>
                    <Text style={[styles.link, { color: '#fff' }]} onPress={() => this.reset()}>Sign Up</Text>
                  </View>
                  {
                    this.state.user !== false &&
                      <Button title='Log In'
                        onPress={() => this.props.navigation.navigate('Login', { user: this.state.user })}
                        buttonStyle={styles.buttonHome}
                        titleStyle={styles.buttonTitleHome}/>
                  }
                </View>
              </View>
            </View>
          </View>
        </ThemeProvider>
      )
    }
  }

  _handleNotification = (notification) => {
    switch (notification.data.type) {
      case 'receive':
        console.log('***** Receive')
        this.props.setnewTx(notification.data)
        this.props.navigation.navigate('newTxPopUp')
        break
      case 'pay':
        console.log('***** Payment')
        // ignore notifications that are not legit
        if (notification.data.data) {
          this.props.setnewPay(notification.data)
          this.props.navigation.navigate('newPayPopUp')
        } else {
          // phantom menace: notifications without necessary data,
          // negative notificationId -- not sure where these are from,
          // but ignore them.
          console.log('_handleNotifications: ignore phantom menace')
          console.log(notification.data)
        }
        break
      case 'transfer':
        console.log('******* New Transfer')
        this.props.getPricesAndBalances(notification.data)
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require('../assets/background.png'),
        require('../assets/logo.png')
      ]),
      Font.loadAsync({
        Lato: require('../assets/fonts/Lato-Regular.ttf'),
        Montserrat: require('../assets/fonts/Montserrat-Regular.ttf'),
        'Montserrat-bold': require('../assets/fonts/Montserrat-Bold.ttf')
      })
    ])
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true })
  };
}

Home.propTypes = {
  isFocused: PropTypes.bool.isRequired,
  setContacts: PropTypes.func.isRequired,
  setUser: PropTypes.func.isRequired,
  setnewTx: PropTypes.func.isRequired,
  getPricesAndBalances: PropTypes.func.isRequired,
  setnewPay: PropTypes.func.isRequired,
  newTransfer: PropTypes.func.isRequired,
  skipLoadingScreen: PropTypes.bool,
  loadAppAndBackendVersions: PropTypes.func
}

const mapStateToProps = state => {
  const { contacts, user } = state
  return {
    user: user,
    contacts: contacts
  }
}

export default connect(mapStateToProps, {
  setUser,
  setContacts,
  setnewTx,
  setnewPay,
  newTransfer,
  getPricesAndBalances,
  loadAppAndBackendVersions
})(withNavigationFocus(Home))
