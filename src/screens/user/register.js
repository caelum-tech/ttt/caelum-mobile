import React from 'react'
import { View, TouchableOpacity, Text, Image } from 'react-native'
import { Input, Button, ThemeProvider, Icon } from 'react-native-elements'
import PropTypes from 'prop-types'

import { connect } from 'react-redux'
import { Notifications } from 'expo'
import * as Permissions from 'expo-permissions'

import UserService from '../../services/user'
import ContactsService from '../../services/contacts'
import { setUser } from '../../actions'
import 'ethers/dist/shims.js'
import { ethers } from 'ethers'

const globalStyles = require('../../utils/styles.js')
const theme = require('../../utils/theme.js')

class Register extends React.Component {
    static navigationOptions = ({ navigation }) => {
      return {
        headerTitle: 'Tetratic',
        headerStyle: { backgroundColor: '#BD2525' },
        headerTitleStyle: { textAlign: 'center', alignSelf: 'center', color: 'white', fontWeight: 'bold', fontFamily: 'Lato' },
        headerLeft: (<TouchableOpacity onPress={() => { navigation.navigate('Home') }}><Image style={{ marginLeft: 10 }} source={require('../../assets/logoBar.png')} /></TouchableOpacity>),
        headerRight: (<View></View>)
      }
    }

    // This becomes the "User" object persisted by UserService
    constructor (props) {
      super(props)
      this.state = {
        step: 0,
        password: '',
        name: '',
        handler: '',
        nonce: 1,
        showError: false,
        token: null
      }
    }

    async componentDidMount () {
      UserService.loadUser().then(async (user) => {
        console.log(user)
        this.setState({ user: user })
        this.setState({ step: (user === false) ? 1 : 0 })
      })
    }

    // Check Notification Permissions
    registerForPushNotificationsAsync = async () => {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      )
      let finalStatus = existingStatus
      // only ask if permissions have not already been determined, because
      // iOS won't necessarily prompt the user a second time.
      if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
        finalStatus = status
      }

      // Stop here if the user did not grant permissions
      if (finalStatus !== 'granted') {
        return
      }

      // Get the token that uniquely identifies this device
      const token = await Notifications.getExpoPushTokenAsync()

      this.setState({ token: token })
    }

    // Submit Handlers

    handleSubmitName () {
      if (this.state.name !== '') {
        this.setState({ showError: false })
        this.setState({ step: 3 })
      } else {
        this.setState({ showError: true })
      }
    }

    handleSubmitPassword () {
      if (this.state.password !== '') {
        this.setState({ showError: false })
        this.setState({ step: 2 })
      } else {
        this.setState({ showError: true })
      }
    }

    async createIdentity () {
      if (this.state.handler.length === 0) {
        this.setState({ showError: true })
        return
      }

      // Create the token for theis app : expo comms.
      await this.registerForPushNotificationsAsync()

      // Remove any existing user.
      await UserService.removeUser()

      console.log('*** Create identity')
      console.log(this.state.handler.toLowerCase())
      ContactsService.getContact(this.state.handler.toLowerCase())
        .then(async (contact) => {
          if (!contact.success) {
            this.setState({ step: 4 })
            setTimeout(async () => {
              const wallet = await ethers.Wallet.createRandom()
              this.setState({ step: 5 })
              UserService.createDid(wallet.address, this.state.handler.toLowerCase(), this.state.token)
                .then((did) => {
                  const userToSave = {
                    name: this.state.name,
                    handler: this.state.handler,
                    password: this.state.password,
                    privateKey: wallet.privateKey,
                    nonce: 100,
                    did: did,
                    expoToken: this.state.token
                  }
                  this.props.setUser(userToSave)
                  UserService.saveUser(userToSave).then((value) => {
                    this.setState({ step: 6 })
                  })
                })
            }, 100)
          } else {
            this.setState({ showError: true })
          }
        })
    }

    showWarning () {
      return (
        <View style={{ alignContent: 'center' }}>
          <View style={{ marginTop: 15, justifyContent: 'center' }}>
            <Text style={[globalStyles.textg, globalStyles.textTop]}>WARNING</Text>
          </View>
          <View style={globalStyles.inputContainerApp}>
            <Text>You already have a user on this Mobile</Text>
            <Text>All previous data will be deleted</Text>
          </View>
          <View style={globalStyles.buttonContainerApp}>
            <Button
              title='Continue'
              buttonStyle={globalStyles.buttonApp}
              titleStyle={globalStyles.buttonTitleApp}
              onPress={() => { this.setState({ step: 1 }) }}
            />
            {this.state.showError === true &&
                <View style={{ marginTop: 10, alignItems: 'center' }}>
                  <Text style={{ color: 'red' }}>This handler is not available.</Text>
                </View>
            }
          </View>
        </View>
      )
    }

    inputHandler () {
      return (
        <View style={{ alignContent: 'center' }}>
          <View style={{ marginTop: 15, justifyContent: 'center' }}>
            <Text style={[globalStyles.textg, globalStyles.textTop]}>Enter your new handler</Text>
          </View>
          <View style={globalStyles.inputContainerApp}>
            <Input
              inputStyle={globalStyles.inputApp}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder='Handler'
              onChangeText = { (text) => this.setState({ handler: text })}
              selectionColor='#FFFFFF'
              underlineColorAndroid='transparent'
            />
          </View>
          <View style={globalStyles.buttonContainerApp}>
            <Button
              title='Continue'
              buttonStyle={globalStyles.buttonApp}
              titleStyle={globalStyles.buttonTitleApp}
              onPress={() => { this.createIdentity() }}
            />
            {this.state.showError === true &&
                <View style={{ marginTop: 10, alignItems: 'center' }}>
                  <Text style={{ color: 'red' }}>This handler is not available.</Text>
                </View>
            }
          </View>
        </View>
      )
    }

    inputName () {
      return (
        <View style={{ alignContent: 'center' }}>
          <View style={{ marginTop: 15, justifyContent: 'center' }}>
            <Text style={[globalStyles.textg, globalStyles.textTop]}>Please enter your Full Name</Text>
          </View>
          <View style={globalStyles.inputContainerApp}>
            <Input
              inputStyle={globalStyles.inputApp}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              containerStyle={{ justifyContent: 'center', width: '85%' }}
              placeholder='Full Name'
              onChangeText = { (text) => this.setState({ name: text })}
              selectionColor='#FFFFFF'
              underlineColorAndroid='transparent'
            />
          </View>
          <View style={globalStyles.buttonContainerApp}>
            <Button
              title='Continue'
              buttonStyle={globalStyles.buttonApp}
              titleStyle={globalStyles.buttonTitleApp}
              onPress={ () => { this.handleSubmitName() } }
            />
            {this.state.showError === true &&
            <View style={{ marginTop: 10, alignItems: 'center' }}>
              <Text style={{ color: 'red' }}>Name should have at least one character</Text>
            </View>
            }
          </View>

        </View>
      )
    }

    inputPassword () {
      return (
        <View style={{ alignContent: 'center' }}>
          <View style={{ marginTop: 15, justifyContent: 'center' }}>
            <Text style={[globalStyles.textg, globalStyles.textTop]}>Please enter your new password</Text>
          </View>

          <View style={globalStyles.inputContainerApp}>
            <Input
              secureTextEntry={true}
              inputStyle={globalStyles.inputApp}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              placeholder='Password'
              onChangeText = { (text) => this.setState({ password: text })}
              selectionColor='#FFFFFF'
              underlineColorAndroid='transparent'
            />
          </View>
          <View style={globalStyles.buttonContainerApp}>
            <Button
              title='Continue'
              buttonStyle={globalStyles.buttonApp}
              titleStyle={globalStyles.buttonTitleApp}
              onPress={ () => { this.handleSubmitPassword() } }
            />
            {this.state.showError === true &&
            <View style={{ marginTop: 10, alignItems: 'center' }}>
              <Text style={{ color: 'red' }}>Password should have at least one character.</Text>
            </View>
            }
          </View>
        </View>
      )
    }

    render () {
      return (
        <ThemeProvider theme={theme}>
          <View style={globalStyles.backgroundContainer}>
            { this.state.step === 0 && this.showWarning()}
            { this.state.step === 1 && this.inputPassword()}
            { this.state.step === 2 && this.inputName()}
            { this.state.step === 3 && this.inputHandler()}
            { this.state.step === 4 &&
                <View style={globalStyles.container_steps}>
                  <Icon name='star-border' color='red' />
                  <Text style={globalStyles.textgh1}>Creating a secure Wallet</Text>
                  <Text style={globalStyles.textg}>This process can take a few seconds</Text>
                  <Text style={globalStyles.textg}>Please wait...</Text>
                </View>
            }
            { this.state.step === 5 &&
                <View style={globalStyles.container_steps}>
                  <Icon name='star-half' color='red'/>
                  <Text style={globalStyles.textgh1}>Creating a new Identity</Text>
                  <Text style={globalStyles.textg}>Your new Digital Identity is on the way</Text>
                  <Text style={globalStyles.textg}>Please wait...</Text>
                </View>
            }
            { this.state.step === 6 &&
                <View style={globalStyles.container_steps}>
                  <Icon name='star' color='red'/>
                  <Text style={globalStyles.textgh1}>Congratulations</Text>
                  <Text style={globalStyles.textg}>Your Identity is now ready!</Text>
                  <View style={globalStyles.buttonContainerApp}>
                    <Button
                      title='Continue'
                      buttonStyle={globalStyles.buttonApp}
                      titleStyle={globalStyles.buttonTitleApp}
                      onPress={() => this.props.navigation.navigate('LoggedIn')}
                    />
                  </View>
                </View>
            }
            <View style={{ width: '100%', marginTop: 35, alignItems: 'center' }}>
              <Image style={{ alignContent: 'center', marginTop: 25 }} source={require('../../assets/ttc_login.png')} />
            </View>
          </View>
        </ThemeProvider>
      )
    }
}

Register.propTypes = {
  setUser: PropTypes.func.isRequired,
  user: PropTypes.object
}

const mapStateToProps = state => {
  const { user } = state.user
  return {
    user: user
  }
}

export default connect(mapStateToProps, { setUser })(Register)
