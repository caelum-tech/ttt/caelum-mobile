import React from 'react'
import { View, TouchableOpacity, Text, Image } from 'react-native'
import { Input, Button, ThemeProvider } from 'react-native-elements'
import UserService from '../../services/user.js'

// Global styles
import globalStyles from '../../utils/styles'
const theme = require('../../utils/theme.js')

export default class Login extends React.Component {
    static navigationOptions = ({ navigation }) => {
      return {
        headerTitle: 'Tetratic',
        headerStyle: { backgroundColor: '#BD2525' },
        headerTitleStyle: { textAlign: 'center', alignSelf: 'center', color: 'white', fontWeight: 'bold', fontFamily: 'Lato' },
        headerLeft: (<TouchableOpacity onPress={() => { navigation.navigate('Home') }}><Image style={{ marginLeft: 10 }} source={require('../../assets/logoBar.png')} /></TouchableOpacity>),
        headerRight: (<View></View>)
      }
    }

    constructor (props) {
      super(props)
      this.state = {
        password: '',
        showError: false,
        user: {}
      }
    }

    componentDidMount () {
      UserService.loadUser().then(async (user) => {
        this.setState({ user: user })
      })
    }

    handlePassword () {
      if (this.state.password === this.state.user.password) {
        this.props.navigation.navigate('Dapps')
      } else {
        this.setState({ showError: true })
      }
    }

    changePassword (text) {
      this.setState({ password: text })
      this.setState({ showError: false })
    }

    render () {
      return (
        <ThemeProvider theme={theme}>
          <View style={globalStyles.backgroundContainer}>
            <View style={{ alignContent: 'center' }}>
              <View style={{ marginTop: 15, justifyContent: 'center' }}>
                <Text style={[globalStyles.textgb, globalStyles.textTop]}>Welcome back to Tetratic</Text>
                <Text style={[globalStyles.textg, globalStyles.textTop]}>Please enter your password</Text>
              </View>
              <View style={globalStyles.inputContainerApp}>
                <Input
                  secureTextEntry={true}
                  placeholder='Password'
                  inputStyle={globalStyles.inputApp}
                  inputContainerStyle={{ borderBottomWidth: 0 }}
                  selectionColor='#FFFFFF'
                  underlineColorAndroid='transparent'
                  onChangeText = { (text) => {
                    this.setState({ password: text })
                    this.setState({ showError: false })
                  }}
                />
              </View>
              <View style={globalStyles.buttonContainerApp}>
                <Button
                  title='Continue'
                  buttonStyle={globalStyles.buttonApp}
                  titleStyle={globalStyles.buttonTitleApp}
                  onPress={() => { this.handlePassword() }}
                />
              </View>
              <View style={{ width: '100%', marginTop: 35, alignItems: 'center' }}>
                <Image style={{ alignContent: 'center', marginTop: 25 }} source={require('../../assets/ttc_login.png')} />
              </View>

              {
                this.state.showError === true &&
                        <View style={{ marginTop: 10, alignItems: 'center' }}>
                          <Text style={{ color: 'red' }}>Invalid Password, please try again.</Text>
                        </View>
              }
            </View>
          </View>

        </ThemeProvider>
      )
    }
}
