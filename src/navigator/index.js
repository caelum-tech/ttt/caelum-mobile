import React from 'react' // eslint-disable-line no-unused-vars
import { createAppContainer, createSwitchNavigator } from 'react-navigation'

import LoggedOutNavigator from './LoggedOut'
import LoggedInNavigator from './LoggedIn'
import TransactionSwitch from '../navigator/LoggedIn/Transactions'

export const RootNavigator = () => createAppContainer(createSwitchNavigator(
  {
    LoggedOut: {
      screen: LoggedOutNavigator
    },
    LoggedIn: {
      screen: LoggedInNavigator
    },
    TransactionSwitch: {
      screen: TransactionSwitch
    }
  },
  {
    initialRouteName: 'LoggedOut'
  }
))
