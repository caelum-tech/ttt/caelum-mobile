import React from 'react' // eslint-disable-line no-unused-vars
import { createStackNavigator } from 'react-navigation-stack'

import Home from '../screens/home'
import Register from '../screens/user/register'
import Login from '../screens/user/login'

const LoggedOutNavigator = createStackNavigator({

  Home: {
    screen: Home
  },
  Register: {
    screen: Register
  },
  Login: {
    screen: Login
  }
},
{
  initialRouteName: 'Home'
})

export default LoggedOutNavigator
