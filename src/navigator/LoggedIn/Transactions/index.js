import React from 'react' // eslint-disable-line no-unused-vars
import { createSwitchNavigator } from 'react-navigation'

// Screens
import SelectContact from '../../../screens/main/Transactions/SelectContact'
import SelectToken from '../../../screens/main/Transactions/SelectToken'
import AddContactToList from '../../../screens/main/Transactions/AddContactToList'
import ConfirmTransaction from '../../../screens/main/Transactions/ConfirmTransaction'
import QrCodeScanner from '../../../screens/main/Transactions/QrCodeScanner'
import TxHistoryScreen from '../../../screens/main/Transactions/TxHistoryScreen'
import txDetail from '../../../screens/main/Transactions/txDetail'
import newTxPopUp from '../../../screens/main/Transactions/NewTxPopUp'
import newPayPopUp from '../../../screens/main/Transactions/NewPayPopUp'

const TransactionSwitch = createSwitchNavigator({
  SelectContact: {
    screen: SelectContact
  },
  SelectToken: {
    screen: SelectToken
  },
  AddContactToList: {
    screen: AddContactToList
  },
  ConfirmTransaction: {
    screen: ConfirmTransaction
  },
  QrCodeScanner: {
    screen: QrCodeScanner
  },
  TxHistoryScreen: {
    screen: TxHistoryScreen
  },
  newTxPopUp: {
    screen: newTxPopUp
  },
  newPayPopUp: {
    screen: newPayPopUp
  },
  txDetail: {
    screen: txDetail
  }
})

export default TransactionSwitch
