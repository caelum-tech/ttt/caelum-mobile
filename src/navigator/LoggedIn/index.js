/* eslint react/prop-types: 0 */

import React from 'react' // eslint-disable-line no-unused-vars
import Icon from 'react-native-vector-icons/Ionicons'

import { createBottomTabNavigator } from 'react-navigation-tabs'

import Dapps from '../../screens/main/dapps'
import About from '../../screens/main/about'
import Contacts from '../../screens/main/contacts'
import QrCodeScannerScreen from '../../screens/main/QrCodeScannerScreen'
import MainTransactionsScreen from '../../screens/main/Transactions/MainTransactionsScreen'

const LoggedInNavigator = createBottomTabNavigator({
  Dapps: {
    screen: Dapps,
    navigationOptions: {
      // eslint-disable-next-line react/display-name
      tabBarIcon: ({ tintColor }) => (
        <Icon name="md-home" size={25} color={tintColor} />
      )
    }
  },
  About: {
    screen: About,
    navigationOptions: {
      // eslint-disable-next-line react/display-name
      tabBarIcon: ({ tintColor }) => (
        <Icon name="md-person" size={25} color={tintColor} />
      )
    }
  },
  Contacts: {
    screen: Contacts,
    navigationOptions: {
      // eslint-disable-next-line react/display-name
      tabBarIcon: ({ tintColor }) => (
        <Icon name="md-reorder" size={25} color={tintColor} />
      )
    }
  },
  MainTransactionsScreen: {
    screen: MainTransactionsScreen,
    navigationOptions: {
      // eslint-disable-next-line react/display-name
      tabBarIcon: ({ tintColor }) => (
        <Icon name="md-swap" size={25} color={tintColor} />
      )
    }
  },
  QrCodeScannerScreen: {
    screen: QrCodeScannerScreen,
    navigationOptions: {
      // eslint-disable-next-line react/display-name
      tabBarIcon: ({ tintColor }) => (
        <Icon name="md-aperture" size={25} color={tintColor} />
      )
    }
  }
},
{
  order: ['Dapps', 'Contacts', 'QrCodeScannerScreen', 'MainTransactionsScreen', 'About'],
  tabBarOptions: {
    activeTintColor: 'tomato',
    inactiveTintColor: 'gray',
    showIcon: true,
    showLabel: false,
    style: {
      backgroundColor: 'white'
    }
  }
})

export default LoggedInNavigator
