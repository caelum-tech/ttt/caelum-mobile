// TransactionSaga.js

import {
  takeEvery,
  put,
  select
} from 'redux-saga/effects'

import {
  INITIATE_TX
} from '../actions/types'

import {
  setTxStatus
} from '../actions'

import EthService from '../services/eth'
import UserService from '../services/user'

const getUserItems = state => state.user
const getTxItems = state => state.transactions

function * buildAndSendTx () {
  // Set Tx Status as Initiated.
  yield put(setTxStatus('INITIATED'))
  // Get State Items.
  const userItems = yield select(getUserItems)
  const txItems = yield select(getTxItems)
  // Deconstruct Items.
  const handler = userItems.handler
  const amount = txItems.amount
  const token = txItems.token.toLowerCase()
  const sendToHandle = txItems.handler.toLowerCase()

  // Get Token Address.
  const tokenAddress = yield UserService.loadTokens(token)

  // Get Receiver DID.
  const toDid = yield UserService.resolveUserDid(sendToHandle)

  // Build and sign transaction.
  const txData = yield EthService.getTransfer(tokenAddress, toDid, amount)

  // Send tx to /transfer endpoint.
  const txStatus = yield EthService.sendTransaction(txData, handler, sendToHandle, token, amount)

  // Handle response.
  if (txStatus.success) {
    // Set txStatus as CONFIRMED
    yield put(setTxStatus('CONFIRMED'))
  } else {
    // Set txStatus as ERROR
    yield put(setTxStatus('ERROR'))
  }
}

export default function * watchInitiateTx () {
  yield takeEvery(INITIATE_TX, buildAndSendTx)
}
