// TransactionSaga.js

import {
  takeEvery,
  //    put,
  select
} from 'redux-saga/effects'

import {
  INITIATE_PAY
} from '../actions/types'

import EthService from '../services/eth'
import UserService from '../services/user'

const getUserItems = state => state.user
const getTxItems = state => state.transactions

function * buildAndSendPay () {
  // Set Tx Status as Initiated.
  // yield put(setTxStatus('INITIATED'))

  // Get State Items.
  const userItems = yield select(getUserItems)
  const txItems = yield select(getTxItems)

  // Deconstruct Items.
  const handler = userItems.handler
  const amount = txItems.amount
  const sendToHandle = txItems.handler.toLowerCase()
  console.log('TRANSFER ' + amount + ' to ' + sendToHandle)

  // Get Token Address.
  const tokenAddress = yield UserService.loadTokens('ttc')
  console.log('TOKEN ttc to ' + tokenAddress)

  // Get Receiver DID.
  const toDid = yield UserService.resolveUserDid(sendToHandle)
  console.log('Root DID ' + toDid)

  // Build and sign transaction.
  const txData = yield EthService.getTransfer(tokenAddress, toDid, amount)
  console.log('txData ' + txData)

  // Send tx to /transfer endpoint.
  const txStatus = yield EthService.sendTransaction(txData, handler, sendToHandle, amount)
  console.log('send TX ' + txStatus)
}

export default function * watchInitiatePay () {
  yield takeEvery(INITIATE_PAY, buildAndSendPay)
}
