import { all } from 'redux-saga/effects'

import watchInitiateTx from './TransactionSaga'
import watchInitiatePay from './PaymentSaga'
import watchLoadTxHistory from './txHistorySaga'
import watchVersionLoad from './VersionsSaga'
import watchBalances from './PricesAndBalancesSaga'
import Auth from './Auth'

export default function * rootSaga () {
  yield all([
    Auth(),
    watchInitiateTx(),
    watchBalances(),
    watchInitiatePay(),
    watchLoadTxHistory(),
    watchVersionLoad()
  ])
}
