// TransactionSaga.js

import {
  takeEvery,
  put,
  select
} from 'redux-saga/effects'

import {
  LOAD_TX_HISTORY
} from '../actions/types'

import {
  setTxHistory,
  setTxHistoryError
} from '../actions'

import UserService from '../services/user'

const getUserItems = state => state.user

function * loadAndSetTxHistory () {
  // Get State Items.
  const userItems = yield select(getUserItems)

  // Deconstruct handler.
  const handler = userItems.handler.toLowerCase()

  // Fetch tx history from endpoint.
  const txHistory = yield UserService.getUserTransactions(handler)

  // Handle response.
  if (txHistory) {
    // Set txStatus as CONFIRMED
    yield put(setTxHistory(txHistory))
  } else {
    // Set txStatus as ERROR
    yield put(setTxHistoryError())
  }
}

export default function * watchLoadTxHistory () {
  yield takeEvery(LOAD_TX_HISTORY, loadAndSetTxHistory)
}
