// PricesAndBalancesSaga.js

import {
  put,
  select,
  takeEvery
} from 'redux-saga/effects'

import {
  GET_PRICES_AND_BALANCES
} from '../actions/types'

import {
  setUserBalance
} from '../actions'

import UserService from '../services/user'

export function * handleBalance () {
  // Initiate loop.
  const user = yield select((state) => state.user)
  console.log(user)
  // Load balance.
  try {
    const balance = yield UserService.getUserBalance(user.handler)
    console.log(balance)
    yield put(setUserBalance(balance))
  } catch (e) {
    // TODO: manage
  }
}

export default function * watchBalances () {
  yield takeEvery(GET_PRICES_AND_BALANCES, handleBalance)
}
