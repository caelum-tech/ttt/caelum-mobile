// VersionsSaga.js

import {
  takeEvery,
  put
} from 'redux-saga/effects'

import {
  LOAD_VERSION
} from '../actions/types'

import {
  setVersionsMatchBoolean
} from '../actions'

import UserService from '../services/user'

function * checkIfVersionsMatch () {
  // Fetch versions endpoint.
  const versions = yield UserService.checkVersionsAreMatching()

  // If versions do NOT match,
  if (versions.error) {
    // Set error variable in reducer to equal TRUE
    yield put(setVersionsMatchBoolean(true))
    // Handle versioning error logic HERE!
    yield console.log("I'm sorry, versions are not matching")
  } else {
    // Set error variable in reducer to equal FALSE
    yield put(setVersionsMatchBoolean(false))
    // Handle following lofic here.
    yield console.log('Versions are matching! Backend version is: ', versions.backendVersion)
  }
}

export default function * watchVersionLoad () {
  yield takeEvery(LOAD_VERSION, checkIfVersionsMatch)
}
