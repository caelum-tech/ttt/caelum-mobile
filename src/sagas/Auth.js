import { fork, put } from 'redux-saga/effects'

import UserService from '../services/user'
import { setUser } from '../actions'

function * asyncLogin () {
  // Load user.
  const user = yield UserService.loadUser()
  // Initiate loop.
  if (user) {
    yield put(setUser(user))
  }
}

export default function * () {
  yield fork(asyncLogin)
}
