// priceAndBalances.js

import {
  GET_PRICES_AND_BALANCES,
  SET_USER_BALANCE,
  NEW_TRANSFER
} from './types'

// First action. Trigers Saga.
export const getPricesAndBalances = () => {
  return {
    type: GET_PRICES_AND_BALANCES,
    payload: null
  }
}

// Set Balance.
export const setUserBalance = balance => {
  return {
    type: SET_USER_BALANCE,
    payload: balance
  }
}

// New Transfer received.
export const newTransfer = tx => {
  return {
    type: NEW_TRANSFER,
    payload: tx
  }
}
