// transactions.js

import { SET_PAY } from './types'

export const setnewPay = pay => {
  return {
    type: SET_PAY,
    payload: pay
  }
}
