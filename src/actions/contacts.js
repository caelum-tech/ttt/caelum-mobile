// contacts.js

import { SET_CONTACTS, SET_CONTACT } from './types'

export const setContacts = contacts => {
  return {
    type: SET_CONTACTS,
    payload: contacts
  }
}

export const setSelectedContact = contact => {
  return {
    type: SET_CONTACT,
    payload: contact
  }
}
