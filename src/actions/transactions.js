// transactions.js

import {
  SET_HANDLER,
  SET_TOKEN,
  SET_AMOUNT,
  SET_STATUS,
  INITIATE_TX,
  CLEAN_STATE
} from './types'

export const cleanReduxState = () => {
  return {
    type: CLEAN_STATE,
    payload: null
  }
}

export const setHandler = handler => {
  return {
    type: SET_HANDLER,
    payload: handler
  }
}

export const setToken = token => {
  return {
    type: SET_TOKEN,
    payload: token
  }
}

export const setAmount = amount => {
  return {
    type: SET_AMOUNT,
    payload: amount
  }
}

export const setTxStatus = status => {
  return {
    type: SET_STATUS,
    payload: status
  }
}

export const initiateTransaction = () => {
  return {
    type: INITIATE_TX,
    payload: null
  }
}
