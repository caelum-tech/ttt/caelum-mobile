// transactions.js

import { SET_TX } from './types'

export const setnewTx = tx => {
  return {
    type: SET_TX,
    payload: tx
  }
}
