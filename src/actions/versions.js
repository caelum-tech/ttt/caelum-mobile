// versions.js

import {
  IS_VERSION_MATCHING,
  LOAD_VERSION
} from './types'

export const loadAppAndBackendVersions = () => {
  return {
    type: LOAD_VERSION,
    payload: null
  }
}

export const setVersionsMatchBoolean = bool => {
  return {
    type: IS_VERSION_MATCHING,
    payload: bool
  }
}
