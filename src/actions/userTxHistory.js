// userTxHistory.js

import {
  LOAD_TX_HISTORY,
  SET_TX_HISTORY,
  SET_TX_HISTORY_ERROR
} from './types'

export const loadTxHistory = () => {
  return {
    type: LOAD_TX_HISTORY,
    payload: null
  }
}

export const setTxHistory = txHistory => {
  return {
    type: SET_TX_HISTORY,
    payload: txHistory
  }
}

export const setTxHistoryError = () => {
  return {
    type: SET_TX_HISTORY_ERROR,
    payload: null
  }
}
