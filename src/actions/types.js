
// Transactions Action Types.
export const SET_HANDLER = 'set_handle'
export const SET_TOKEN = 'set_token'
export const SET_AMOUNT = 'set_amount'
export const CLEAN_STATE = 'clean_state'
export const SET_STATUS = 'set_status'
export const INITIATE_TX = 'initiate_tx'

// Payment Action Types.
export const SET_PAYMENT = 'set_payment'
export const INITIATE_PAY = 'initiate_payment'

// Contact Action Types.
export const SET_CONTACTS = 'set_contacts'
export const SET_CONTACT = 'set_contact'

// User Action Types.
export const SET_USER = 'set_user'

// New Tx Types.
export const SET_TX = 'set_tx'
export const SET_PAY = 'set_pay'

// Prices and Balances Types.
export const GET_PRICES_AND_BALANCES = 'get_prices_and_balances'
export const SET_USER_BALANCE = 'set_user_balance'

// Tx History Types.
export const LOAD_TX_HISTORY = 'load_tx_history'
export const SET_TX_HISTORY = 'set_tx_history'
export const SET_TX_HISTORY_ERROR = 'set_tx_history_error'

// Version Matching Types.
export const IS_VERSION_MATCHING = 'is_version_matching'
export const LOAD_VERSION = 'load_version'
