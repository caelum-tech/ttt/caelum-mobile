// transactions.js

import {
  SET_PAYMENT,
  // SET_STATUS,
  INITIATE_PAY
} from './types'

export const setPayment = payment => {
  return {
    type: SET_PAYMENT,
    payload: payment
  }
}

export const initiatePayment = () => {
  return {
    type: INITIATE_PAY,
    payload: null
  }
}
