
// GENERAL HELPER FUNCTIONS //

const helpers = {
  // Returns a string with a capitalized initial.
  CapitalizeInitial: function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }
}

export default helpers
