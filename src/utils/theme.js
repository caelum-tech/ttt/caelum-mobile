module.exports = {
  primaryColor: 'white',
  Button: {
    titleStyle: {
      fontFamily: 'Lato',
      color: '#FFFFFF',
      textAlign: 'center'
    },
    buttonStyle: {
      height: 40,
      backgroundColor: '#b72027',
      marginTop: 20
    },
    containerStyle: {
      width: '85%'
    }
  },
  Text: {
    style: {
      color: '#FFFFFF'
    }
  },
  headerStyle: {
    textAlign: 'center',
    alignSelf: 'center',
    color: 'black',
    fontWeight: 'bold',
    fontFamily: 'Lato'
  },
  headerTitle: {
    backgroundColor: '#000'
  }
}
