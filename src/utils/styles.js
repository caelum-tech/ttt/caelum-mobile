/* eslint no-unused-vars: 0, import/first: 0 */
'use strict'

var React = require('react-native')
import EStyleSheet from 'react-native-extended-stylesheet'

module.exports = EStyleSheet.create({
  backgroundContainer: {
    flex: 1,
    resizeMode: 'cover',
    width: undefined,
    height: undefined,
    backgroundColor: '#F4F4F4'
  },
  container: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container2: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center'
  },
  container_steps: {
    marginTop: 40,
    width: '100%',
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CCC',
    borderColor: '#666'
  },
  itemList: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 5,
    borderColor: '#DCDCDC',
    borderWidth: 1,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    height: 90,
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: '#777',
    shadowOffset: { height: 5, width: 5 }
  },
  textList: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 5,
    borderColor: '#DCDCDC',
    borderWidth: 1,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 10,
    paddingTop: 10,
    marginBottom: 10,
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: '#777',
    shadowOffset: { height: 5, width: 5 }
  },
  itemLeft: {
    flex: 2,
    textAlign: 'center',
    alignItems: 'center'
  },
  itemRight: {
    textAlign: 'left',
    flex: 7
  },
  body: {
    flex: 4,
    width: '90%',
    fontSize: 20,
    marginBottom: 15,
    textAlign: 'left'
  },
  bodyImg: {
    flex: 4,
    width: '90%',
    marginBottom: 15,
    alignItems: 'center'
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: 16,
    alignItems: 'center',
    width: '100%'
  },
  buttonBottom: {
    marginTop: 10
  },
  home: {
    flex: 3,
    width: '100%',
    alignItems: 'center'
  },
  texth1: {
    fontFamily: 'Lato',
    color: 'white',
    fontSize: 24,
    marginTop: 35,
    marginBottom: 16,
    textAlign: 'center'
  },
  text: {
    fontFamily: 'Lato',
    color: 'white',
    fontSize: 16,
    textAlign: 'center'
  },
  textg: {
    fontFamily: 'Montserrat',
    color: '#666',
    fontSize: 20
  },
  textgb: {
    fontFamily: 'Montserrat-bold',
    color: '#333',
    fontSize: 20
  },
  block: {
    paddingLeft: 20,
    paddingRight: 20,
    marginTop: 15,
    textAlign: 'center'
  },
  texts: {
    fontFamily: 'Lato',
    color: '#666',
    fontSize: 12
  },
  textgh1: {
    marginTop: 5,
    marginBottom: 5,
    fontFamily: 'Montserrat-bold',
    fontSize: 24,
    color: '#333',
    textAlign: 'center'
  },
  textgh2: {
    marginTop: 5,
    fontFamily: 'Montserrat',
    color: '#333',
    fontSize: 16
  },
  textgh3: {
    marginTop: 15,
    fontFamily: 'Montserrat',
    color: '#333',
    fontSize: 20
  },
  textGreen: {
    color: '#54B541'
  },
  textYellow: {
    color: '#EFAD5C'
  },
  textRed: {
    color: '#e60000'
  },
  textTop: {
    marginTop: 25,
    textAlign: 'center'
  },
  link: {
    fontFamily: 'Montserrat',
    color: '#444',
    fontSize: 14,
    textDecorationLine: 'underline'
  },
  textInput: {
    color: '#333'
  },
  button: {
    paddingTop: 10
  },
  fillView: {
    marginTop: 100,
    alignItems: 'center',
    width: '100%'
  },
  pad: {
    paddingLeft: 15
  },
  pad2: {
    paddingLeft: 35,
    paddingRight: 35,
    textAlign: 'center'
  },
  // Hugo Styles.
  // Containers:
  containerStyle: {
    flex: 1,
    paddingTop: 35,
    flexDirection: 'column',
    backgroundColor: '#fff'
  },
  simpleContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#fff'
  },
  centeredContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  // Text:
  textStyle: {
    fontSize: 16,
    color: '#8E1D1A',
    marginLeft: 10,
    marginBottom: 15
  },
  textStyle22: {
    fontSize: 22,
    color: '#8E1D1A',
    marginLeft: 10
  },
  textBoldRed14: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#8E1D1A'
  },
  titleStyleOne: {
    fontSize: 22,
    alignSelf: 'center',
    color: '#8E1D1A'
  },
  buttonStyle: {
    padding: 15,
    borderRadius: 10,
    backgroundColor: '#8E1D1A',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.25,
    shadowRadius: 6.84,
    elevation: 10
  },
  transparentButtonTitle: {
    fontWeight: 'bold',
    color: '#8E1D1A'
  },
  modalDarkBackground: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)'
  },
  listItem: {
    marginTop: 7,
    marginRight: 15,
    marginLeft: 15,
    marginBottom: 7,
    backgroundColor: 'white',
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  baseContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  listHeaderBox: {
    flexDirection: 'row',
    justifyContent: 'center',
    height: '10%',
    borderBottomWidth: 1,
    borderColor: '#cecece',
    margin: 15
  },
  avatarStyle: {
    flex: 2,
    flexDirection: 'column',
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    marginLeft: 50,
    marginBottom: 10
  },
  buttonHome: {
    height: 50,
    borderRadius: 15,
    borderWidth: 1.5,
    borderColor: 'white',
    backgroundColor: 'transparent'
  },
  buttonTitleHome: {
    fontFamily: 'Montserrat'
  },
  inputContainerApp: {
    width: '100%',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 30,
    paddingLeft: 30
  },
  inputApp: {
    textAlign: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 55,
    borderWidth: 1,
    borderColor: '#999',
    borderRadius: 10,
    backgroundColor: '#FFFFFF',
    fontFamily: 'Montserrat'
  },
  buttonContainerApp: {
    width: '100%',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 30,
    paddingLeft: 30
  },
  buttonApp: {
    backgroundColor: '#BD2525',
    shadowColor: '#EEE',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.25,
    shadowRadius: 6.84,
    elevation: 10,
    width: '100%',
    height: 55,
    borderRadius: 10
  },
  buttonTitleApp: {
    textAlign: 'center',
    justifyContent: 'center',
    color: '#FFFFFF',
    fontFamily: 'Montserrat'
  },
  formContainer: {
    width: '100%',
    backgroundColor: '#FFF',
    marginTop: 30,
    marginBottom: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  boxContainer: {
    flex: 1.5,
    margin: 20,
    alignItems: 'center',
    justifyContent: 'center'
  }
})
