import React, { Component } from 'react'
import { Image } from 'react-native'
const styles = require('../utils/styles.js')

class BackgroundImage extends Component {
  render () {
    return (
      <Image source={require('../assets/background.png')}
        style={styles.backgroundImage}>
        {this.props.children}
      </Image>
    )
  }
}

export default BackgroundImage
