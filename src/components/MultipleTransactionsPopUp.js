import React, { Component } from 'react'
import { View, Text } from 'react-native'
import { Button } from 'react-native-elements'
import PropTypes from 'prop-types'

import PopUp from './common/PopUp'
import styles from '../utils/styles'

// Props:
// @newTxs
// @firstButtonFunction
// @SecondButtonFunction

export default class MultipleTransactionsPopUp extends Component {
  render () {
    const {
      container,
      headerContainer,
      textStyle,
      numberStyle,
      buttonContainer,
      transparentButton
    } = compStyles
    return (
      <View>
        <PopUp iconName={'bell'} title={'  NEW TRANSACTIONS!'}>
          <View style={ container }>
            <View style={ headerContainer }>
              <Text style={ textStyle }>You have
                <Text style={ numberStyle }>
                  { ' ' + this.props.newTxs.length }
                </Text> new transactions, do you want to check them out?
              </Text>
            </View>
            <View style={ buttonContainer }>
              <Button
                title='No Thanks'
                onPress={ this.props.firstButtonFunction }
                titleStyle={ styles.transparentButtonTitle }
                buttonStyle={ transparentButton }
              />

              <Button
                title='OK'
                onPress={ this.props.secondButtonFunction }
                buttonStyle={ styles.buttonStyle }
              />
            </View>
          </View>
        </PopUp>
      </View>
    )
  }
}

const compStyles = {
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  headerContainer: {
    flex: 1,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  textStyle: {
    fontSize: 18
  },
  numberStyle: {
    fontSize: 16,
    alignSelf: 'center',
    fontWeight: 'bold',
    color: '#8E1D1A'
  },
  buttonContainer: {
    flex: 2,
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  transparentButton: {
    backgroundColor: 'transparent',
    marginBottom: 30
  }
}

MultipleTransactionsPopUp.propTypes = {
  newTxs: PropTypes.array.isRequired,
  firstButtonFunction: PropTypes.func.isRequired,
  secondButtonFunction: PropTypes.func.isRequired
}
