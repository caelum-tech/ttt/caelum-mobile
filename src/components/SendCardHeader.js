import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import IconContainer from '../components/common/IconContainer'
import PropTypes from 'prop-types'

// Props:
// @tokenIcon
// @onPressChange: to select a different token.

const SendCardHeader = props => {
  return (
    <View style={ styles.firstContainerStyle }>
      <View style={ styles.secondContainerStyle}>
        <View style={ styles.tokenIconContainer }>
          <IconContainer
            flex={1}
            image={props.tokenIcon}
            imageSize={65}
          />
        </View>

        <View style={ styles.titleContainer }>
          {
            props.children
          }
        </View>

        <TouchableOpacity
          style={ styles.changeIconContainer }
          onPress={props.onPressChange}
        >
          <Icon name="md-repeat" size={30} color={'#8E1D1A'} />
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = {
  firstContainerStyle: {
    flex: 2,
    flexDirection: 'row',
    marginTop: 5,
    marginLeft: 4,
    marginRight: 4,
    backgroundColor: '#E2E2E2',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15
  },
  secondContainerStyle: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row'
  },
  tokenIconContainer: {
    flex: 1,
    margin: 10,
    marginTop: 10,
    alignSelf: 'flex-start'
  },
  titleContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch'
  },
  changeIconContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch'
  }
}

SendCardHeader.propTypes = {
  tokenIcon: PropTypes.node,
  onPressChange: PropTypes.func
}

export default SendCardHeader
