import React from 'react'
import { View, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import PropTypes from 'prop-types'

import UserAvatarWithNameAndHandle from './common/UserAvatarWithNameAndHandle'

// Props:
// senderName: string
// senderHandle: string
// receiverName: string
// receiverHandle: string

const FromMeToYouHeader = props => {
  return (
    <View style={ styles.containerStyle }>
      <UserAvatarWithNameAndHandle
        name={props.senderName}
        handle={props.senderHandle}
      />
      <Icon
        name="md-arrow-round-forward"
        size={25} color={'#8E1D1A'}
        style={{ marginTop: 10, marginRight: 20 }}
      />
      <UserAvatarWithNameAndHandle
        name={props.receiverName}
        handle={props.receiverHandle}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: '15%',
    marginTop: '10%'
  },
  textStyle: {
    fontSize: 16,
    color: '#8E1D1A',
    marginLeft: 10,
    marginBottom: 15
  }

})

FromMeToYouHeader.propTypes = {
  senderName: PropTypes.string.isRequired,
  senderHandle: PropTypes.string.isRequired,
  receiverHandle: PropTypes.string.isRequired,
  receiverName: PropTypes.string.isRequired
}

export default FromMeToYouHeader
