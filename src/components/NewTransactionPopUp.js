import React, { Component } from 'react'
import { View, Text, Dimensions } from 'react-native'
import { Button } from 'react-native-elements'
import EStyleSheet from 'react-native-extended-stylesheet'
import PropTypes from 'prop-types'

import PopUp from './common/PopUp'
import styles from '../utils/styles'

// Props:
// @tx
// @firstButtonFunction
// @SecondButtonFunction

const width = Dimensions.get('window').width

EStyleSheet.build({
  $rem: width / 370
})

export default class NewTransactionPopUp extends Component {
  render () {
    const {
      container,
      headerContainer,
      textStyle,
      numberStyle,
      buttonContainer,
      transparentButton
    } = compStyles

    return (
      <View>
        <PopUp iconName={'bell'} title={'  NEW TRANSACTION!'}>
          <View style={ container }>
            <View style={ headerContainer }>
              <Text style={ textStyle }>You have
                <Text style={ numberStyle }> one
                </Text> new transaction from
                <Text style={ numberStyle }>{ ' ' + this.props.tx.handler2 }
                </Text>, do you want to check it out?
              </Text>
            </View>
            <View style={ buttonContainer }>
              <Button
                title='No Thanks'
                onPress={ this.props.firstButtonFunction }
                titleStyle={ styles.transparentButtonTitle }
                buttonStyle={ transparentButton }
              />

              <Button
                title='OK'
                onPress={ this.props.secondButtonFunction }
                buttonStyle={ styles.buttonStyle }
              />
            </View>
          </View>
        </PopUp>
      </View>
    )
  }
}

const compStyles = EStyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  headerContainer: {
    flex: 1,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  textStyle: {
    fontSize: '18rem'
  },
  numberStyle: {
    fontSize: '18rem',
    alignSelf: 'center',
    fontWeight: 'bold',
    color: '#8E1D1A'
  },
  buttonContainer: {
    flex: 2,
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10
  },
  transparentButton: {
    backgroundColor: 'transparent',
    marginBottom: 30
  }
})

NewTransactionPopUp.propTypes = {
  tx: PropTypes.object.isRequired,
  firstButtonFunction: PropTypes.func.isRequired,
  secondButtonFunction: PropTypes.func.isRequired
}
