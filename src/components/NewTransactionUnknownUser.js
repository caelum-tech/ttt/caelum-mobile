import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions
} from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
import EStyleSheet from 'react-native-extended-stylesheet'
import PropTypes from 'prop-types'
import PopUp from './common/PopUp'

// Props:
// @tx
// @firstButtonFunction
// @SecondButtonFunction
// @thirdButtonFunction

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

export default class NewTransactionUnknownUser extends Component {
  render () {
    const {
      textStyle,
      buttonStyle,
      buttonTextInWhite,
      buttonTextInRed
    } = styles
    return (
      <View>
        <PopUp iconName={'bell'} title={'  NEW TRANSACTION!'}>
          <View style={{ flex: 1, flexDirection: 'column' }}>
            <View style={{ flex: 1, padding: 25 }}>
              <Text style={ textStyle }>You have
                <Text style={{ fontWeight: 'bold', color: '#8E1D1A' }}>
                  {' '}one
                </Text> new transaction from
                <Text style={{ fontWeight: 'bold', color: '#8E1D1A' }}>
                  { ' ' + this.props.tx.handler2.charAt(0).toUpperCase() + this.props.tx.handler2.slice(1) }
                </Text>
              </Text>
            </View>

            <View style={{ flex: 2 }}>
              <View style={{ flex: 1, alignItems: 'center' }}>
                <TouchableOpacity
                  onPress={ this.props.firstButtonFunction }
                  style={ buttonStyle }>
                  <Icon name="plus" size={width / 15} color={'white'} />
                  <Text>{'  '}</Text>
                  <Text style={ buttonTextInWhite }> Add Contact </Text>
                </TouchableOpacity>
              </View>

              <View style={{ flex: 1, flexDirection: 'row', borderTopWidth: 1, borderColor: '#8E1D1A' }}>
                <TouchableOpacity
                  onPress={ this.props.secondButtonFunction }
                  style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={ buttonTextInRed }> Check </Text>
                  <Text style={ buttonTextInRed }> Transaction </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={ this.props.thirdButtonFunction }
                  style={{ flex: 1, justifyContent: 'center', alignItems: 'center', borderLeftWidth: 1, borderColor: '#8E1D1A' }}>
                  <Text style={ buttonTextInRed }> No </Text>
                  <Text style={ buttonTextInRed }> Thanks </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </PopUp>
      </View>
    )
  }
}

const styles = EStyleSheet.create({
  textStyle: {
    fontSize: '18rem',
    textAlign: 'center'
  },
  buttonStyle: {
    width: width / 2,
    height: height / 18,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#8E1D1A',
    borderRadius: 25
  },
  buttonTextInWhite: {
    fontSize: '18rem',
    fontWeight: 'bold',
    color: 'white'
  },
  buttonTextInRed: {
    fontSize: '18rem',
    fontWeight: 'bold',
    color: '#8E1D1A'
  }
})

NewTransactionUnknownUser.propTypes = {
  tx: PropTypes.object.isRequired,
  firstButtonFunction: PropTypes.func.isRequired,
  secondButtonFunction: PropTypes.func.isRequired,
  thirdButtonFunction: PropTypes.func.isRequired
}

EStyleSheet.build({
  $rem: width / 370
})
