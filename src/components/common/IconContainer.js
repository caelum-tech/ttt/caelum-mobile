import React from 'react'
import { View, Image } from 'react-native'
import PropTypes from 'prop-types'

// Props:
// @flex
// @image
// @imageSize

const IconContainer = props => {
  return (
    <View style={ [styles.containerStyle, { flex: props.flex }] }>
      <View style={ styles.imageContainer }>
        <Image source={ props.image } style={{ width: props.imageSize, height: props.imageSize }}/>
      </View>
    </View>
  )
}

const styles = {
  containerStyle: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageContainer: {
    backgroundColor: '#fff',
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10
  }
}

IconContainer.propTypes = {
  flex: PropTypes.number.isRequired,
  imageSize: PropTypes.number.isRequired,
  image: PropTypes.node
}

export default IconContainer
