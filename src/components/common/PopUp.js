import React from 'react'
import {
  View,
  Text,
  Modal,
  Dimensions
} from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
import EStyleSheet from 'react-native-extended-stylesheet'
import PropTypes from 'prop-types'

// Props:
// @ - iconName
// @ - title
// @ - children

EStyleSheet.build({
  $fontColor: 'green',
  $bgColor: '#e6e6e6',
  $rem: 16
})

const PopUp = props => {
  const dimWidth = Dimensions.get('window').width
  const iconSize = dimWidth / 13
  return (
    <View>
      <Modal
        animationType="slide"
        transparent={false}
        visible={true}
        onRequestClose={() => {}}>
        <View style={ componentStyles.containerStyle }>
          <View style={ componentStyles.darkBackground }>
            <View style={ componentStyles.popUpBox }>
              <View style={ componentStyles.header }>
                <Icon name={props.iconName} size={iconSize} color={'#C7C7C7'} />
                <Text style={ componentStyles.titleStyle}>{ props.title }</Text>
              </View>
              <View style={ componentStyles.body }>
                {
                  props.children
                }
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  )
}

const componentStyles = EStyleSheet.create({
  containerStyle: {
    height: '100%',
    width: '100%',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center'
  },
  darkBackground: {
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(0,0,0,0.8)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  popUpBox: {
    height: '50%',
    width: '80%',
    flexDirection: 'column',
    alignSelf: 'center',
    backgroundColor: '#FFF',
    borderRadius: 25
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  body: {
    flex: 3
  },
  titleStyle: {
    fontSize: '1.3rem',
    fontWeight: 'bold',
    color: '#8E1D1A'
  }
})

PopUp.propTypes = {
  iconName: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
}

export default PopUp
