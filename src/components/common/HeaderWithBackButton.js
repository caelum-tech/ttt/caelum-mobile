import React from 'react'
import {
  View,
  Text,
  TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
import PropTypes from 'prop-types'

import GS from '../../utils/styles'

// Props:
// onBackButtonPress: function

const HeaderWithBackButton = props => {
  return (
    <View style={ styles.container }>
      <TouchableOpacity
        onPress={ props.onBackButtonPress }
        style={ styles.buttonContainer }>
        <Icon name="arrow-left" size={30} color={'#000'} />
      </TouchableOpacity>

      <Text style={ styles.textStyle }>
        {
          props.children
        }
      </Text>
    </View>
  )
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
    borderBottomWidth: 1,
    borderColor: '#cecece'
  },
  buttonContainer: {
    height: 30,
    width: 30,
    marginTop: 20,
    marginLeft: 15
  },
  textStyle: [
    GS.titleStyleOne,
    {
      fontWeight: 'bold',
      marginTop: 20,
      marginLeft: 50
    }
  ]
}

HeaderWithBackButton.propTypes = {
  onBackButtonPress: PropTypes.func
}

export default HeaderWithBackButton
