import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import ContactsService from '../../services/contacts'

// Props
// @name
// @handle
// @size

const UserAvatarWithNameAndHandle = props => {
  const {
    containerStyle,
    textContainer,
    firstTextStyle,
    secondtextStyle
  } = styles

  return (
    <View style={ containerStyle }>

      <Image
        source={ContactsService.getAvatar(props.handle)}
        style={{
          alignContent: 'center',
          alignSelf: 'center',
          marginLeft: 20,
          marginTop: 15,
          width: props.size || 40,
          height: props.size || 40,
          borderRadius: props.size / 2 || 20
        }}
      />

      <View style={ textContainer }>
        <Text style={ firstTextStyle }>{props.name}</Text>
        <Text style={ secondtextStyle }> @{props.handle}</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textContainer: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 15,
    marginBottom: 0
  },
  firstTextStyle: {
    alignSelf: 'center',
    marginLeft: 0,
    marginBottom: 0,
    marginTop: 14,
    fontSize: 14,
    color: '#8E1D1A'
  },
  secondtextStyle: {
    alignSelf: 'center',
    marginLeft: 0,
    marginBottom: 0,
    marginTop: 0,
    fontSize: 12,
    color: 'gray'
  }
})

UserAvatarWithNameAndHandle.propTypes = {
  size: PropTypes.number,
  name: PropTypes.string.isRequired,
  handle: PropTypes.string.isRequired
}

export default UserAvatarWithNameAndHandle
