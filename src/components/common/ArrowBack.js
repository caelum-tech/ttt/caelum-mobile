import React from 'react'
import { TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import PropTypes from 'prop-types'

const ArrowBack = props => {
  return (
    <TouchableOpacity
      style={ styles.arrowStyle }
      onPress={ props.onPress }
    >
      <Icon name="md-arrow-back" size={25} color={'black'} />
    </TouchableOpacity>
  )
}

const styles = {
  arrowStyle: {
    width: 30,
    height: 30,
    borderRadius: 15,
    marginTop: 35,
    marginLeft: 15,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  }
}

ArrowBack.propTypes = {
  onPress: PropTypes.func
}

export default ArrowBack
