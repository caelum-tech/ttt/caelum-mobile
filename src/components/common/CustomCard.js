import React from 'react'
import { View } from 'react-native'
import PropTypes from 'prop-types'

// Props:
// @styles

const CustomCard = props => {
  return (
    <View style={[styles.containerStyle, props.style]}>
      {
        props.children
      }
    </View>
  )
}

const styles = {
  containerStyle: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  }
}

CustomCard.propTypes = {
  style: PropTypes.object
}

export default CustomCard
