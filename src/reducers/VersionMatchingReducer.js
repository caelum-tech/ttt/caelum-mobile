// VersionMatchingReducer.js

import {
  IS_VERSION_MATCHING
} from '../actions/types'

const initialState = {
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case IS_VERSION_MATCHING:
      return { error: action.payload }
    default:
      return state
  }
}
