// transactionsReducer.js

import {
  SET_HANDLER,
  SET_TOKEN,
  SET_AMOUNT,
  SET_STATUS,
  CLEAN_STATE
} from '../actions/types'

const initialState = {
  handler: null,
  token: null,
  amount: null,
  txStatus: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_HANDLER:
      return { ...state, handler: action.payload }
    case SET_TOKEN:
      return { ...state, token: action.payload }
    case SET_AMOUNT:
      return { ...state, amount: action.payload }
    case SET_STATUS:
      return { ...state, txStatus: action.payload }
    case CLEAN_STATE:
      return { ...initialState }
    default:
      return state
  }
}
