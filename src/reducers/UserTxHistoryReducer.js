// UserTxHistoryReducer.js

import {
  SET_TX_HISTORY,
  SET_TX_HISTORY_ERROR
} from '../actions/types'

const initialState = {
  transactionList: null,
  loading: true,
  error: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_TX_HISTORY:
      return { ...state, transactionList: action.payload.tx, loading: false }
    case SET_TX_HISTORY_ERROR:
      return { ...state, error: true }
    default:
      return state
  }
}
