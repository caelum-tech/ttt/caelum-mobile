// PricesAndBalancesReducers.js

import {
  SET_USER_BALANCE
} from '../actions/types'

const initialState = {
  ttcBalance: 0,
  usdaBalance: 0,
  tokens: [],
  loading: true
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_BALANCE:
      return { ...state, ttcBalance: action.payload[0].balance, usdaBalance: action.payload[1].balance, tokens: action.payload, loading: false }
    default:
      return state
  }
}
