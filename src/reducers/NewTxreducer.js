// NewTxReducer.js

import { SET_TX } from '../actions/types'

const initialState = {
  newTx: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_TX:
      return { ...state, newTx: action.payload }
    default:
      return state
  }
}
