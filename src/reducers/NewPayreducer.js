// NewPayReducer.js

import { SET_PAY } from '../actions/types'

const initialState = {
  newPay: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PAY:
      return { ...state, newPay: action.payload }
    default:
      return state
  }
}
