// contactReducer.js

import { SET_CONTACTS, SET_CONTACT } from '../actions/types'

const initialState = {
  contacts: [],
  selectedContact: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CONTACTS:
      return { ...state, contacts: action.payload }
    case SET_CONTACT:
      return { ...state, selectedContact: action.payload }
    default:
      return state
  }
}
