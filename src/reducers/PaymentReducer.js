// paymentReducer.js

import {
  SET_PAYMENT
  // SET_STATUS,
  // CLEAN_STATE,
} from '../actions/types'

const initialState = {
  payment: {
    buyer: '',
    seller: '',
    buyToken: '',
    buyAmount: 0,
    buyType: '',
    buyDecimals: 0,
    payToken: '',
    payAmount: 0,
    payPrice: 0
  }
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PAYMENT:
      return { ...state, payment: action.payload }
    /* case SET_AMOUNT:
      return { ...state, amount: action.payload }
    case SET_STATUS:
      return { ...state, txStatus: action.payload }
    case CLEAN_STATE:
      return { ...initialState } */
    default:
      return state
  }
}
