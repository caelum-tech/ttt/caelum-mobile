import { combineReducers } from 'redux'

import ContactsReducer from './ContactsReducer'
import UserReducer from './UserReducer'
import PaymentReducer from './PaymentReducer'
import TransactionsReducer from './TransactionsReducer'
import NewTxReducer from './NewTxreducer'
import NewPayReducer from './NewPayreducer'
import PricesAndBalancesReducer from './PricesAndBalancesReducer'
import UserTxHistoryReducer from './UserTxHistoryReducer'
import VersionsMatchingReducer from './VersionMatchingReducer'

export default combineReducers({
  pricesAndBalances: PricesAndBalancesReducer,
  transactions: TransactionsReducer,
  payment: PaymentReducer,
  contacts: ContactsReducer,
  user: UserReducer,
  newTx: NewTxReducer,
  newPay: NewPayReducer,
  txHistory: UserTxHistoryReducer,
  version: VersionsMatchingReducer
})
