import { AsyncStorage } from 'react-native'
import { CAELUM_WORKER_API_URL as apiUrl } from 'react-native-dotenv'

// Touch this file to make sure the .env file is loaded for production compilation

const ContactService = {
  getContact: function (handler) {
    console.log(apiUrl + '/identity/user/' + handler)
    return new Promise(resolve => {
      console.log(apiUrl + '/identity/user/' + handler)
      fetch(apiUrl + '/identity/user/' + handler, { method: 'GET' })
        .then(res => res.json())
        .then(response => resolve(response))
        .catch(error => resolve(error))
    })
  },
  loadContacts: function () {
    return new Promise((resolve, reject) => {
      try {
        AsyncStorage.getItem('contacts').then((values) => {
          if (values === null) {
            resolve(false)
          }
          const json = JSON.parse(values)
          resolve(json)
        })
      } catch (error) {
        reject(error)
      }
    })
  },
  saveContactsInMemory: function (contacts) {
    return new Promise((resolve) => {
      try {
        AsyncStorage.setItem('contacts', JSON.stringify(contacts))
        resolve(true)
      } catch (error) {
        resolve(error)
      }
    })
  },
  getAvatar (handler) {
    let avatar
    if (handler === 'root') {
      avatar = require('../assets/contacts/root.png')
    } else if (handler === 'add') {
      avatar = require('../assets/plus.png')
    } else {
      avatar = require('../assets/contacts/1.png')
    }
    return avatar
  }

}

export default ContactService
