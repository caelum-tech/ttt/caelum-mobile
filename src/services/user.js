import { AsyncStorage } from 'react-native'
import { CAELUM_WORKER_API_URL as apiUrl } from 'react-native-dotenv'
import packageLock from '../../package-lock.json'
const appVersion = packageLock.version

// Touch this file to make sure the .env file is loaded for production compilation

// Calls API endpoints for user services
const UserService = {
  checkVersionsAreMatching () {
    return new Promise(resolve => {
      fetch(apiUrl + '/system/', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ appVersion: appVersion })
      })
        .then(res => res.json())
        .then(response => resolve(response))
        .catch(error => resolve(error))
    })
  },
  loadUser () {
    return new Promise((resolve, reject) => {
      try {
        AsyncStorage.getItem('user_ttt').then((values) => {
          if (values === null) {
            resolve(false)
          }
          const json = JSON.parse(values)
          resolve(json)
        })
      } catch (error) {
        reject(error)
      }
    })
  },
  saveUser (user) {
    return new Promise((resolve) => {
      try {
        AsyncStorage.setItem('user_ttt', JSON.stringify(user))
        resolve(true)
      } catch (error) {
        resolve(false)
      }
    })
  },
  removeUser () {
    return new Promise((resolve) => {
      try {
        AsyncStorage.removeItem('user_ttt')
        resolve(true)
      } catch (error) {
        resolve(false)
      }
    })
  },
  createDid (address, handler, expoToken) {
    return fetch(apiUrl + '/identity', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ address, handler, expoToken })
    }).then((response) => response.json())
      .then((json) => json.address)
  },
  loadTokens (handler) {
    return fetch(apiUrl + '/identity/token/' + handler)
      .then((resp) => resp.json())
      .then((json) => json.success)
  },
  resolveUserDid (handler) {
    return fetch(apiUrl + '/identity/user/' + handler)
      .then(res => res.json())
      .then(json => json.success)
  },
  getUserBalance (handler) {
    return new Promise(resolve => {
      console.log('Ask for balance to ' + apiUrl + '/identity/balance/' + handler)
      fetch(apiUrl + '/identity/balance/' + handler)
        .then(res => resolve(res.json()))
        .catch(error => resolve(error))
    })
  },
  getUserTransactions (handler) {
    return new Promise(resolve => {
      fetch(apiUrl + '/identity/transfers/' + handler)
        .then(res => res.json())
        .then(response => {
          resolve(response)
        })
        .catch(error => resolve(error))
    })
  }
}

export default UserService
