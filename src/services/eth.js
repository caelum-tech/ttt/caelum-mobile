import UserService from './user'
import { CAELUM_WORKER_API_URL as apiUrl } from 'react-native-dotenv'
import { ethers } from 'ethers'
// Import ERC725 abi
import Identity from '../contracts/Identity.json'

// Touch this file to make sure the .env file is loaded for production compilation

// Ethereum-related API endpoints
const EthService = {
  getTransfer: async function (tokenAddr, didTo, total) {
    return new Promise(async (resolve, reject) => { // eslint-disable-line no-async-promise-executor
      // Load User
      const user = await UserService.loadUser()
      // Create Wallet object with privateKey
      const wallet = new ethers.Wallet(user.privateKey)
      // Get nonce (we manage this ourselves since we're the only originators
      // of transactions from this address)
      const nonce = user.nonce
      // Increment nonce for the next transaction and save it.
      user.nonce++
      UserService.saveUser(user)

      // Get ERC725 contract interface
      const iface = new ethers.utils.Interface(Identity.abi)
      // Encode function arguments
      const args = Object.values({ tokenAddr, didTo, total })
      // Encode transaction data
      const calldata = iface.functions.transfer.encode(args)
      // Construct transaction object to be signed
      const transaction = {
        nonce: nonce,
        gasLimit: 800000,
        to: user.did,
        data: calldata
      }
      // Sign transaction object
      const signPromise = await wallet.sign(transaction)
      console.log('*****************************************')
      console.log(signPromise)
      // Resolve signature
      resolve(signPromise)
    })
  },
  sendTransaction: function (data, handler, sendToHandle, token, amount) {
    console.log(apiUrl)
    return fetch(apiUrl + '/identity/transfer', {
      method: 'POST',
      body: JSON.stringify({ signedTx: data, from: handler, to: sendToHandle, token: token, value: amount }),
      headers: { 'Content-Type': 'application/json' }
    })
      .then(res => {
        return { success: res.ok }
      })
  },
  sendPayment: function (data, payment) {
    console.log(apiUrl)
    return fetch(apiUrl + '/identity/payment', {
      method: 'POST',
      body: JSON.stringify({
        signedTx: data,
        buyer: payment.buyer,
        seller: payment.seller,
        buyToken: payment.buyToken,
        buyAmount: payment.buyAmount,
        buyType: payment.buyType,
        buyDecimals: payment.buyDecimals,
        payToken: payment.payToken,
        payAmount: payment.payAmount,
        payPrice: payment.payPrice,
        data: payment.data
      }),
      headers: { 'Content-Type': 'application/json' }
    })
      .then(res => res.json())
  }
}

export default EthService
