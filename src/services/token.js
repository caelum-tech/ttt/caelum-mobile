const TokenService = {
  getAvatar (type, token) {
    let icon
    if (type === 'add') {
      icon = require('../assets/plus.png')
    } else if (type === 'system') {
      icon = require('../assets/icons/TokenSystem.png')
    } else if (type === 'money') {
      if (token === 'usda') {
        icon = require('../assets/icons/USDAicon.png')
      } else {
        icon = require('../assets/icons/TokenMoney.png')
      }
    } else if (type === 'food') {
      icon = require('../assets/icons/TokenFood.png')
    } else if (type === 'tickets') {
      icon = require('../assets/icons/TokenTickets.png')
    } else {
      icon = require('../assets/icons/TokenKeys.png')
    }
    return icon
  }

}

export default TokenService
