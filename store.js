// store.js

import { createStore, combineReducers } from 'redux'
import contactReducer from './reducers/contactReducer'

const rootReducer = combineReducers({
  contacts: contactReducer
})

const configureStore = () => {
  return createStore(rootReducer)
}

export default configureStore
