/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { configure } from 'enzyme'
import configureStore from 'redux-mock-store'

import * as priceAndBalancesActions from '../../src/actions/priceAndBalances'
import {
  GET_PRICES_AND_BALANCES,
  SET_USER_BALANCE
} from '../../src/actions/types'

configure({ adapter: new Adapter() })

const mockStore = configureStore()
const store = mockStore()

describe('Price And Balances Actions', () => {
  beforeEach(() => {
    store.clearActions()
  })
})

describe('First Action, triggers Saga', () => {
  beforeEach(() => {
    store.clearActions()
  })

  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: GET_PRICES_AND_BALANCES,
        payload: null
      }
    ]

    store.dispatch(priceAndBalancesActions.getPricesAndBalances())
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(priceAndBalancesActions.getPricesAndBalances())
    expect(store.getActions()).toMatchSnapshot()
  })
})

describe('Set User Balance Action', () => {
  beforeEach(() => {
    store.clearActions()
  })

  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: SET_USER_BALANCE,
        payload: 1892
      }
    ]

    store.dispatch(priceAndBalancesActions.setUserBalance(1892))
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(priceAndBalancesActions.setUserBalance(1892))
    expect(store.getActions()).toMatchSnapshot()
  })
})
