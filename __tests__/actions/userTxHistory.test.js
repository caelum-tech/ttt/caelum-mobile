/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { configure } from 'enzyme'
import configureStore from 'redux-mock-store'

import * as userTxHistory from '../../src/actions/userTxHistory'

configure({ adapter: new Adapter() })

const mockStore = configureStore()
const store = mockStore()

const mockTxList = {
  tx: {
    handler: 'Pepito',
    handler2: 'Pepita',
    value: 1873,
    token: 'ttc',
    type: 'send',
    createdAt: '0x4744b435'
  }
}

describe('User Tx History Actions', () => {
  beforeEach(() => {
    store.clearActions()
  })

  describe('Load User Tx History Action', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedAction = [
        {
          type: 'load_tx_history',
          payload: null
        }
      ]

      store.dispatch(userTxHistory.loadTxHistory())
      expect(store.getActions()).toEqual(expectedAction)
    })

    test('Matches Snapshot', () => {
      store.dispatch(userTxHistory.loadTxHistory())
      expect(store.getActions()).toMatchSnapshot()
    })
  })

  describe('Set Tx History Action', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedAction = [
        {
          type: 'set_tx_history',
          payload: mockTxList
        }
      ]

      store.dispatch(userTxHistory.setTxHistory(mockTxList))
      expect(store.getActions()).toEqual(expectedAction)
    })

    test('Matches Snapshot', () => {
      store.dispatch(userTxHistory.setTxHistory(mockTxList))
      expect(store.getActions()).toMatchSnapshot()
    })
  })

  describe('Set Tx History Error Action', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedAction = [
        {
          type: 'set_tx_history_error',
          payload: null
        }
      ]

      store.dispatch(userTxHistory.setTxHistoryError())
      expect(store.getActions()).toEqual(expectedAction)
    })

    test('Matches Snapshot', () => {
      store.dispatch(userTxHistory.setTxHistoryError())
      expect(store.getActions()).toMatchSnapshot()
    })
  })
})
