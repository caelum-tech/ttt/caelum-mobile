/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { configure } from 'enzyme'
import configureStore from 'redux-mock-store'

import * as contactsActions from '../../src/actions/contacts'
import contacts from '../../src/assets/contacts.json'
import {
  SET_CONTACTS,
  SET_CONTACT
} from '../../src/actions/types'

configure({ adapter: new Adapter() })

const mockStore = configureStore()
const store = mockStore()

const mockContact = {
  name: 'Pepito',
  handler: 'Pepito',
  address: '0x4744b435'
}

describe('Contacts Actions', () => {
  beforeEach(() => {
    store.clearActions()
  })
})

describe('Set Contacts Action', () => {
  beforeEach(() => {
    store.clearActions()
  })

  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: SET_CONTACTS,
        payload: contacts
      }
    ]

    store.dispatch(contactsActions.setContacts(contacts))
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(contactsActions.setContacts(contacts))
    expect(store.getActions()).toMatchSnapshot()
  })
})

describe('Set Specific Contact Action', () => {
  beforeEach(() => {
    store.clearActions()
  })

  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: SET_CONTACT,
        payload: mockContact
      }
    ]

    store.dispatch(contactsActions.setSelectedContact(mockContact))
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(contactsActions.setSelectedContact(mockContact))
    expect(store.getActions()).toMatchSnapshot()
  })
})
