/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { configure } from 'enzyme'
import configureStore from 'redux-mock-store'

import * as versionActions from '../../src/actions/versions'

configure({ adapter: new Adapter() })

const mockStore = configureStore()
const store = mockStore()

describe('User Actions', () => {
  beforeEach(() => {
    store.clearActions()
  })

  describe('Load Version Saga', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedAction = [
        {
          type: 'load_version',
          payload: null
        }
      ]

      store.dispatch(versionActions.loadAppAndBackendVersions())
      expect(store.getActions()).toEqual(expectedAction)
    })

    test('Matches Snapshot', () => {
      store.dispatch(versionActions.loadAppAndBackendVersions())
      expect(store.getActions()).toMatchSnapshot()
    })
  })

  describe('Set Matching Boolean', () => {
    test('Dispatches the correct action and payload', () => {
      const expectedAction = [
        {
          type: 'is_version_matching',
          payload: true
        }
      ]

      store.dispatch(versionActions.setVersionsMatchBoolean(true))
      expect(store.getActions()).toEqual(expectedAction)
    })

    test('Matches Snapshot', () => {
      store.dispatch(versionActions.setVersionsMatchBoolean(true))
      expect(store.getActions()).toMatchSnapshot()
    })
  })
})
