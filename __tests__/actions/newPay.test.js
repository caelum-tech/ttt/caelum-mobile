/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { configure } from 'enzyme'
import configureStore from 'redux-mock-store'

import * as newPayActions from '../../src/actions/newPay'

configure({ adapter: new Adapter() })

const mockStore = configureStore()
const store = mockStore()

const mockPay = {
  handler: 'Pepito',
  value: 1873,
  token: 'ttc',
  price: 10,
  type: 'payment',
  createdAt: '0x4744b435'
}

describe('New Pay Actions', () => {
  beforeEach(() => {
    store.clearActions()
  })
})

describe('New Pay Actions', () => {
  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: 'set_pay',
        payload: mockPay
      }
    ]

    store.dispatch(newPayActions.setnewPay(mockPay))
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(newPayActions.setnewPay(mockPay))
    expect(store.getActions()).toMatchSnapshot()
  })
})
