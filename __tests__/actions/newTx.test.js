/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { configure } from 'enzyme'
import configureStore from 'redux-mock-store'

import * as newTxActions from '../../src/actions/newTx'

configure({ adapter: new Adapter() })

const mockStore = configureStore()
const store = mockStore()

const mockTx = {
  handler: 'Pepito',
  handler2: 'Pepita',
  value: 1873,
  token: 'ttc',
  type: 'send',
  createdAt: '0x4744b435'
}

describe('New Tx Actions', () => {
  beforeEach(() => {
    store.clearActions()
  })
})

describe('New Tx Actions', () => {
  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: 'set_tx',
        payload: mockTx
      }
    ]

    store.dispatch(newTxActions.setnewTx(mockTx))
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(newTxActions.setnewTx(mockTx))
    expect(store.getActions()).toMatchSnapshot()
  })
})
