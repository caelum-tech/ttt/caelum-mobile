/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { configure } from 'enzyme'
import configureStore from 'redux-mock-store'

import * as userActions from '../../src/actions/user'

configure({ adapter: new Adapter() })

const mockStore = configureStore()
const store = mockStore()

const mockUser = {
  user: {
    did: '0xff81B0492c38dF200e01838B0c01BEc4CDa904C4',
    expoToken: 'ExponentPushToken[NvBmMwOYVZ55FjnbBqk_Q_]',
    handler: 'Alberto',
    name: 'Alberto',
    password: '1234',
    nonce: 1,
    privateKey: '0x4744b435f197517c34e14170397d4cbc6819ee5a6770954d930c0b38aff0e369'
  }
}

describe('User Actions', () => {
  beforeEach(() => {
    store.clearActions()
  })
})

describe('Set User Action', () => {
  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: 'set_user',
        payload: mockUser
      }
    ]

    store.dispatch(userActions.setUser(mockUser))
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(userActions.setUser(mockUser))
    expect(store.getActions()).toMatchSnapshot()
  })
})
