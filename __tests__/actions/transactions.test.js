/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { configure } from 'enzyme'
import configureStore from 'redux-mock-store'

import * as transactionsActions from '../../src/actions/transactions'
import {
  SET_HANDLER,
  SET_TOKEN,
  SET_AMOUNT,
  SET_STATUS,
  INITIATE_TX,
  CLEAN_STATE
} from '../../src/actions/types'

configure({ adapter: new Adapter() })

const mockStore = configureStore()
const store = mockStore()

describe('Transactions Actions', () => {
  beforeEach(() => {
    store.clearActions()
  })
})

describe('First Action, triggers Saga', () => {
  beforeEach(() => {
    store.clearActions()
  })

  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: INITIATE_TX,
        payload: null
      }
    ]

    store.dispatch(transactionsActions.initiateTransaction())
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(transactionsActions.initiateTransaction())
    expect(store.getActions()).toMatchSnapshot()
  })
})

describe('Set Handler Action', () => {
  beforeEach(() => {
    store.clearActions()
  })

  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: SET_HANDLER,
        payload: 'Pepito'
      }
    ]

    store.dispatch(transactionsActions.setHandler('Pepito'))
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(transactionsActions.setHandler('Pepito'))
    expect(store.getActions()).toMatchSnapshot()
  })
})

describe('Set Amount Action', () => {
  beforeEach(() => {
    store.clearActions()
  })

  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: SET_AMOUNT,
        payload: 200
      }
    ]

    store.dispatch(transactionsActions.setAmount(200))
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(transactionsActions.setAmount(200))
    expect(store.getActions()).toMatchSnapshot()
  })
})

describe('Set Transaction Status Action', () => {
  beforeEach(() => {
    store.clearActions()
  })

  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: SET_STATUS,
        payload: 'CONFIRMED'
      }
    ]

    store.dispatch(transactionsActions.setTxStatus('CONFIRMED'))
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(transactionsActions.setTxStatus('CONFIRMED'))
    expect(store.getActions()).toMatchSnapshot()
  })
})

describe('Clean State Action', () => {
  beforeEach(() => {
    store.clearActions()
  })

  test('Dispatches the correct action and payload', () => {
    const expectedAction = [
      {
        type: CLEAN_STATE,
        payload: null
      }
    ]

    store.dispatch(transactionsActions.cleanReduxState())
    expect(store.getActions()).toEqual(expectedAction)
  })

  test('Matches Snapshot', () => {
    store.dispatch(transactionsActions.cleanReduxState())
    expect(store.getActions()).toMatchSnapshot()
  })
})
