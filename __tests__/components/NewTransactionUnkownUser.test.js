/* eslint no-undef: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'
import toJson from 'enzyme-to-json'

import NewTransactionUnknownUser from '../../src/components/NewTransactionUnknownUser'

configure({ adapter: new Adapter() })

// UnkownUser Component test
describe('NewTransactionUnknownUser', () => {
  describe('Rendering', () => {
    test('renders the component', () => {
      const wrapper = shallow(<NewTransactionUnknownUser
        tx={{ handler2: 'Hello' }}
        firstButtonFunction={() => null}
        secondButtonFunction={() => null}
        thirdButtonFunction={() => null}
      />)
      const component = wrapper.dive()
      expect(toJson(component)).toMatchSnapshot()
    })
  })
})
