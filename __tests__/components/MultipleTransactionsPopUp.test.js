/* eslint no-undef: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'
import toJson from 'enzyme-to-json'

import MultipleTransactionsPopUp from '../../src/components/MultipleTransactionsPopUp'

configure({ adapter: new Adapter() })

// PopUp Component test
describe('MultipleTransactionsPopUp', () => {
  describe('Rendering', () => {
    test('renders the component', () => {
      const wrapper = shallow(<MultipleTransactionsPopUp
        newTxs={[1, 2, 3]}
        firstButtonFunction={() => null}
        secondButtonFunction={() => null}
      />)
      const component = wrapper.dive()
      expect(toJson(component)).toMatchSnapshot()
    })
  })
})
