/* eslint no-undef: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'
import toJson from 'enzyme-to-json'

import SendCardHeader from '../../src/components/SendCardHeader'

configure({ adapter: new Adapter() })

// UnkownUser Component test
describe('SendCardHeader', () => {
  describe('Rendering', () => {
    test('renders the component', () => {
      const wrapper = shallow(<SendCardHeader onPressChange={() => null} />)
      const component = wrapper.dive()
      expect(toJson(component)).toMatchSnapshot()
    })
  })
})
