/* eslint no-undef: 0 */ // --> OFF

// 0xe34ad925685b1da719452f98fabdf47f07e8b4a2d5d926bc4a5339d229a79dff

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'

// Components import
import PopUp from '../../src/components/common/PopUp'
import ArrowBack from '../../src/components/common/ArrowBack'
import CustomCard from '../../src/components/common/CustomCard'
import IconContainer from '../../src/components/common/IconContainer'
import UserAvatarWithNameAndHandle from '../../src/components/common/UserAvatarWithNameAndHandle'

configure({ adapter: new Adapter() })

// PopUp Component test
describe('PopUp', () => {
  describe('Rendering', () => {
    it('should match to snapshot', () => {
      const component = shallow(<PopUp iconName="bell" title="Hello" />)
      expect(component).toMatchSnapshot()
    })
  })
})

// ArrowBack Component test
describe('ArrowBack', () => {
  describe('Rendering', () => {
    it('should match to snapshot', () => {
      const component = shallow(<ArrowBack onPress={() => null} />)
      expect(component).toMatchSnapshot()
    })
  })
})

// CustomCard Component test
describe('CustomCard', () => {
  describe('Rendering', () => {
    it('should match to snapshot', () => {
      const component = shallow(<CustomCard />)
      expect(component).toMatchSnapshot()
    })
  })
})

// IconContainer Component test
describe('IconContainer', () => {
  describe('Rendering', () => {
    it('should match to snapshot', () => {
      const component = shallow(<IconContainer flex={1} imageSize={20}/>)
      expect(component).toMatchSnapshot()
    })
  })
})

// UserAvatarWithNameAndHandle Component test
describe('UserAvatarWithNameAndHandle', () => {
  describe('Rendering', () => {
    it('should match to snapshot', () => {
      const component = shallow(<UserAvatarWithNameAndHandle
        size={40}
        name={'Perico'}
        handle={'Periquito'}/>)
      expect(component).toMatchSnapshot()
    })
  })
})
