/* eslint no-undef: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'
import toJson from 'enzyme-to-json'

import NewPaymentPopUp from '../../src/components/NewPaymentPopUp'

configure({ adapter: new Adapter() })

// PopUp Component test
describe('NewPaymentPopUp', () => {
  describe('Rendering', () => {
    test('renders the component', () => {
      const wrapper = shallow(<NewPaymentPopUp
        pay={{}}
        firstButtonFunction={() => null}
        secondButtonFunction={() => null}
      />)
      const component = wrapper.dive()
      expect(toJson(component)).toMatchSnapshot()
    })
  })
})
