/* eslint no-undef: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'
import toJson from 'enzyme-to-json'

import FromMeToYouHeader from '../../src/components/FromMeToYouHeader'

configure({ adapter: new Adapter() })

// PopUp Component test
describe('FromMeToYouHeader', () => {
  describe('Rendering', () => {
    test('renders the component', () => {
      const wrapper = shallow(<FromMeToYouHeader
        senderName={'Hello'}
        senderHandle={'Hello'}
        receiverName={'Hello'}
        receiverHandle={'Hello'}
      />)
      const component = wrapper.dive()
      expect(toJson(component)).toMatchSnapshot()
    })
  })
})
