/* eslint no-undef: 0 */ // --> OFF

import React from 'react'
import Adapter from 'enzyme-adapter-react-16'
import { shallow, configure } from 'enzyme'
import toJson from 'enzyme-to-json'

import NewTransactionPopUp from '../../src/components/NewTransactionPopUp'

configure({ adapter: new Adapter() })

// PopUp Component test
describe('NewTransactionPopUp', () => {
  describe('Rendering', () => {
    test('renders the component', () => {
      const wrapper = shallow(<NewTransactionPopUp
        tx={{}}
        firstButtonFunction={() => null}
        secondButtonFunction={() => null}
      />)
      const component = wrapper.dive()
      expect(toJson(component)).toMatchSnapshot()
    })
  })
})
