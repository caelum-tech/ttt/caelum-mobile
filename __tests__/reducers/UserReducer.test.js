/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'

import UserReducer from '../../src/reducers/UserReducer'
import {
  SET_USER
} from '../../src/actions/types'

const mockUser = {
  did: '0xff81B0492c38dF200e01838B0c01BEc4CDa904C4',
  expoToken: 'ExponentPushToken[NvBmMwOYVZ55FjnbBqk_Q_]',
  handler: 'Alberto',
  name: 'Alberto',
  password: '1234',
  privateKey: '0x4744b435f197517c34e14170397d4cbc6819ee5a6770954d930c0b38aff0e369'
}

describe('User Reducer', () => {
  describe('INITIAL_STATE', () => {
    test('Is correct', () => {
      const action = { type: 'dummy' }
      const initialState = {}

      expect(UserReducer(undefined, action)).toEqual(initialState)
    })
  })

  describe('SET_USER', () => {
    test('returns the correct state', () => {
      const action = { type: SET_USER, payload: mockUser }

      expect(UserReducer(undefined, action)).toEqual(mockUser)
    })
  })
})
