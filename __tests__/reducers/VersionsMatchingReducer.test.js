/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'

import VersionMatchingReducer from '../../src/reducers/VersionMatchingReducer'

import {
  IS_VERSION_MATCHING
} from '../../src/actions/types'

describe('Version Matching Reducer', () => {
  describe('INITIAL_STATE', () => {
    test('Is correct', () => {
      const action = { type: 'dummy' }
      const initialState = { error: null }

      expect(VersionMatchingReducer(undefined, action)).toEqual(initialState)
    })
  })

  describe('IS_VERSION_MATCHING', () => {
    test('returns the correct state', () => {
      const action = { type: IS_VERSION_MATCHING, payload: true }
      const expectedState = { error: true }

      expect(VersionMatchingReducer(undefined, action)).toEqual(expectedState)
    })
  })
})
