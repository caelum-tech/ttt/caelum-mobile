/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'

import PricesAndBalancesReducer from '../../src/reducers/PricesAndBalancesReducer'
import {
  SET_USER_BALANCE
} from '../../src/actions/types'

const initialState = {
  ttcBalance: 0,
  usdaBalance: 0,
  tokens: [],
  loading: true
}

describe('Price And Balances Reducer', () => {
  describe('INITIAL_STATE', () => {
    test('Is correct', () => {
      const action = { type: 'dummy' }

      expect(PricesAndBalancesReducer(undefined, action)).toEqual(initialState)
    })
  })

  describe('SET_USER_BALANCE', () => {
    test('returns the correct state', () => {
      const action = { type: SET_USER_BALANCE, payload: [{ token: 'ttc', balance: 1873 }, { token: 'usda', balance: 145 }] }
      const expectedState = { ...initialState, ttcBalance: 1873, usdaBalance: 145, tokens: [{ balance: 1873, token: 'ttc' }, { balance: 145, token: 'usda' }], loading: false }

      expect(PricesAndBalancesReducer(undefined, action)).toEqual(expectedState)
    })
  })
})
