/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'

import ContactsReducer from '../../src/reducers/ContactsReducer'
import contacts from '../../src/assets/contacts.json'
import {
  SET_CONTACTS,
  SET_CONTACT
} from '../../src/actions/types'

describe('Contacts Reducer', () => {
  describe('INITIAL_STATE', () => {
    test('Is correct', () => {
      const action = { type: 'dummy' }
      const initialState = { contacts: [], selectedContact: null }

      expect(ContactsReducer(undefined, action)).toEqual(initialState)
    })
  })

  describe('SET_CONTACTS', () => {
    test('returns the correct state', () => {
      const action = { type: SET_CONTACTS, payload: contacts }
      const expectedState = { contacts: action.payload, selectedContact: null }

      expect(ContactsReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('SET_CONTACT', () => {
    test('returns the correct state', () => {
      const action = { type: SET_CONTACT, payload: { name: 'Pepito', handler: 'Pepito', address: '0x87H..' } }
      const expectedState = { contacts: [], selectedContact: action.payload }

      expect(ContactsReducer(undefined, action)).toEqual(expectedState)
    })
  })
})
