/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'

import TransactionsReducer from '../../src/reducers/TransactionsReducer'
import {
  SET_HANDLER,
  SET_TOKEN,
  SET_AMOUNT,
  SET_STATUS,
  CLEAN_STATE
} from '../../src/actions/types'

const initialState = {
  handler: null,
  token: null,
  amount: null,
  txStatus: null
}

describe('Transactions Reducer', () => {
  describe('INITIAL_STATE', () => {
    test('Is correct', () => {
      const action = { type: 'dummy' }

      expect(TransactionsReducer(undefined, action)).toEqual(initialState)
    })
  })

  describe('SET_HANDLER', () => {
    test('returns the correct state', () => {
      const action = { type: SET_HANDLER, payload: 'Pepito' }
      const expectedState = { ...initialState, handler: 'Pepito' }

      expect(TransactionsReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('SET_AMOUNT', () => {
    test('returns the correct state', () => {
      const action = { type: SET_AMOUNT, payload: 1234 }
      const expectedState = { ...initialState, amount: 1234 }

      expect(TransactionsReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('SET_STATUS', () => {
    test('returns the correct state', () => {
      const action = { type: SET_STATUS, payload: 'Confirmed' }
      const expectedState = { ...initialState, txStatus: 'Confirmed' }

      expect(TransactionsReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('CLEAN_STATE', () => {
    test('returns the correct state', () => {
      const action = { type: CLEAN_STATE, payload: null }

      expect(TransactionsReducer(undefined, action)).toEqual(initialState)
    })
  })
})
