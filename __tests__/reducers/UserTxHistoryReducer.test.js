/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'

import UserTxHistoryReducer from '../../src/reducers/UserTxHistoryReducer'
import {
  SET_TX_HISTORY,
  SET_TX_HISTORY_ERROR
} from '../../src/actions/types'

const mockTxList = {
  tx: {
    handler: 'Pepito',
    handler2: 'Pepita',
    value: 1873,
    token: 'ttt',
    type: 'send',
    createdAt: '0x4744b435'
  }
}

const initialState = {
  transactionList: null,
  loading: true,
  error: false
}

describe('User Tx History Reducer', () => {
  describe('INITIAL_STATE', () => {
    test('Is correct', () => {
      const action = { type: 'dummy' }

      expect(UserTxHistoryReducer(undefined, action)).toEqual(initialState)
    })
  })

  describe('SET_TX_HISTORY', () => {
    test('returns the correct state', () => {
      const action = { type: SET_TX_HISTORY, payload: mockTxList }
      const expectedState = { transactionList: mockTxList.tx, loading: false, error: false }

      expect(UserTxHistoryReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('SET_TX_HISTORY_ERROR', () => {
    test('returns the correct state', () => {
      const action = { type: SET_TX_HISTORY_ERROR, payload: null }
      const expectedState = { ...initialState, error: true }

      expect(UserTxHistoryReducer(undefined, action)).toEqual(expectedState)
    })
  })
})
