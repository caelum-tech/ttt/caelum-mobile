/* eslint no-undef: 0, no-unused-vars: 0 */ // --> OFF

import React from 'react'

import NewTxreducer from '../../src/reducers/NewTxreducer'
import {
  SET_TX
} from '../../src/actions/types'

const mockTx = {
  handler: 'Pepito',
  handler2: 'Pepita',
  value: 1873,
  token: 'ttt',
  type: 'send',
  createdAt: '0x4744b435'
}

describe('NewTx Reducer', () => {
  describe('INITIAL_STATE', () => {
    test('Is correct', () => {
      const action = { type: 'dummy' }
      const initialState = { newTx: null }

      expect(NewTxreducer(undefined, action)).toEqual(initialState)
    })
  })

  describe('SET_TX', () => {
    test('returns the correct state', () => {
      const action = { type: SET_TX, payload: mockTx }
      const expectedState = { newTx: mockTx }

      expect(NewTxreducer(undefined, action)).toEqual(expectedState)
    })
  })
})
