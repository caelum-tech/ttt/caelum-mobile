|Branch|Pipeline|Coverage|
|:-:|:-:|:-:|
|[`master`](https://gitlab.com/caelum-tech/caelum-mobile/tree/master)|[![pipeline status](https://gitlab.com/caelum-tech/caelum-mobile/badges/master/pipeline.svg)](https://gitlab.com/caelum-tech/caelum-mobile/commits/master)|[![coverage report](https://gitlab.com/caelum-tech/caelum-mobile/badges/master/coverage.svg)](https://gitlab.com/caelum-tech/caelum-mobile/commits/master)|

# Prerequisites

* Ubuntu 18.04
* [NVM](https://github.com/nvm-sh/nvm)
` wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash`
* `nvm install 10`
* `sudo apt install build-essential`
* `npm i`

# Automated tests
* `npm run eslint`

# Local testing
Note: you can use your phone to connect to a backend caelum-worker running on your desktop, but your phone and desktop have to be on the same network, and that network has to allow peer-to-peer connections.  In the office, the wifi network "caelumlabs" does allow it, and the other network does not.
* Change the value of `CAELUM_WORKER_API_URL` in `.env` to your local IP and port for caelum-worker.
* `npm run android`

# Debugging with Expo
You can do interactive debugging of the application using Visual Studio Code.  [This article](https://github.com/microsoft/vscode-react-native/blob/master/doc/expo.md) explains how; you will need to install react-native-cli globally (`npm install -g react-native-cli`) and install the IDE plugin.

# Building and Publishing with expo
* `npm install -d expo-cli`

* If you receive "Error: ENOSPC: System limit for number of file watchers reached", 
`echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p`

* The `.env` file has to be set properly for the intended environment at the time of build, and cached Expo files can cause the wrong API endpoint to be called.  To avoid this, `npm run reset-expo`.

## To build APK files for manual manual install
* npm run build-android-apk

## To build an Android App Bundle for publishing on Google Play
* npm run build-android-play

# CI
* https://docs.expo.io/versions/latest/guides/setting-up-continuous-integration/
