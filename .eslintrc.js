module.exports = {
  'env': {
    'browser': true,
    'es6': true
  },
  'extends': [
    'standard',
    'eslint:recommended',
    'plugin:react/recommended'
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly'
  },
  'parser': 'babel-eslint',
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true
    },
    'ecmaVersion': 2018,
    'sourceType': 'module'
  },
  'plugins': [
    'react'
  ],
  'rules': {
    'react/jsx-uses-vars': 'error',
    "react/prop-types": ["error", { "ignore": ["navigation", "children"] }],
    "no-mixed-spaces-and-tabs": 0,
    'no-console': 'off'
  },
  'settings': {
    'react': {
      'version': 'detect'
    }
  }
}

